require(shinyFiles)
require(shinythemes)
require(DT)
require(shinyBS)
require(shinyjs)
require(fcsresfun)
# Define UI for application that draws a histogram

jscode <- "shinyjs.closeWindow = function() { window.close(); }"

shinyUI(fluidPage(
  titlePanel('', windowTitle = 'FCSCalibration v0.4.3'),
  useShinyjs(),
  extendShinyjs(text = jscode, functions = c("closeWindow")),
  #headerPanel('FCS Calibrated imaging'),
  #tags$style(type='text/css', ".action-button{font-size:100%; height:80%}")
  # help messages
  bsTooltip(id = 'batch', title = " Search for all files two levels below main directory",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = "refresh", title = "Click to reload res files",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = "close", title = "Close current session",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = "directory", title = "Click to load directory",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = "session", title = "Name of .res file",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = 'forcebasel', title = "Impose baseline from WT or fit baseline from complete dataset",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = 'dyename', title = "Name of directory containing dye calibration data",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = 'poiname', title = "Name of directories containing POI data",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = 'wtname', title = "Name of directories containing WT/negative control data",
            placement = "bottom", trigger = "hover"),
  bsTooltip(id = 'mfpname', title = "Name of directories containing mFP data",
            placement = "bottom", trigger = "hover"),
  fluidRow(
    #controls data to load and excluded points
    column(4,
           tabsetPanel(
             tabPanel('Input',
                      tags$hr(),
                      fluidRow(
                        column(12,verbatimTextOutput('maindir'))),
                      fluidRow(column(4,
                                      if(Sys.info()['sysname'] == 'Darwin') {
                                        shinyDirButton('directory', 'Folder select', 'Please select a folder')
                                      } else {
                                        actionButton('directory', '', icon = icon("folder-open"))
                                      }),
                               column(4,checkboxInput('batch', 'Batch', value = FALSE)),
                               column(4,actionButton('close', '', icon = icon("power-off"), onclick = "setTimeout(function(){closeWindow},500);"))
                      ),
                      fluidRow(column(10, textInput('session', 'Session', '2c_opt')),
                               column(2,actionButton('refresh', '', icon = icon("refresh"))),
                               tags$style(type='text/css', "#refresh { width:100%; margin-top: 25px}")
                      ),
                      fluidRow(column(3, textInput('dyename', 'Dye', 'Alexa|dye')),
                               column(3, textInput('poiname', 'POI', 'POI|POI_meta|LSM')),
                               column(3, textInput('mfpname', 'mFP', 'mFP|GFP|mEGFPC1|mEGFPN1')),
                               column(3, textInput('wtname', 'WT', 'WT'))
                      ),
                      fluidRow(column(10,fileInput('readresf', 'Load file list')),column(2,
                                                                                         actionButton('refreshfile', '', icon = icon("refresh"))),
                               tags$style(type='text/css', "#refreshfile { width:100%; margin-top: 25px}")),
                      radioButtons("channel", "Channel",
                                   c("Ch1" = "1",
                                     "Ch2" = "2"), inline = T),


                      tags$hr(),
                      DT::dataTableOutput('tablefilepath'),
                      tags$hr(),
                      actionButton('report', 'Report'),
                      actionButton('reportAll', 'Report All'),
                      downloadButton('writefilelist', 'Write file list'),
                      textOutput('report_task'),
                      tags$h4('Used files'),
                      tags$h5('Globalfile'),
                      verbatimTextOutput('resglobalfile'),
                      tags$h5('dye'),
                      verbatimTextOutput('resdyefile'),
                      tags$h5('WT'),
                      verbatimTextOutput('reswtfile'),
                      tags$h5('mFP'),
                      verbatimTextOutput('resmfpfile'),
                      tags$h5('POI'),
                      verbatimTextOutput('respoifile')
             ),
             tabPanel('ROI/fit',
                      tags$hr(),
                      selectInput('classpoi', 'FCS point POI', c('nuc', 'cyt', 'all'), selected = 'all'),
                      selectInput('classmFP', 'FCS point mFP', c('nuc', 'cyt', 'all'), selected = 'all'),

                      selectInput('imgroi', 'ROI size', c('1', '3','5', '9'),  selected='9'),
                      selectInput('fit', 'Linear fit', c('rlm', 'lm')),
                      checkboxInput('cpmscale', 'scale with CPM of mFP', value = TRUE, width = NULL),
                      checkboxInput('usemFP', 'use mFP', value = TRUE, width = NULL),
                      checkboxInput('forcebasel', 'Force baseline', value = TRUE, width = NULL )
             ),
             tabPanel('Filtering',
                      tags$hr(),
                      textInput('annotation', 'Exclude annotated', value = "x", width = NULL, placeholder = NULL),
                      column(2, checkboxInput('R2cb', NULL, value = TRUE, width = NULL)),
                      column(10,sliderInput('R2', 'R2',  min=0.5, max = 1, value=c(0.9,1), step=0.01)),
                      column(2, checkboxInput('Chi2cb', NULL, value = TRUE, width = NULL)),
                      column(10,sliderInput('Chi2', 'Chi2', min=0, max=2, value=1.5, step=0.01)),
                      column(2, checkboxInput('Blcb', NULL, value = TRUE, width = NULL)),
                      column(10,sliderInput('Bleaching', 'Bleaching', min=0.5, max=5, value=c(0.9,5), step=0.1)),
                      column(2, checkboxInput('ratioOffcb', NULL, value = TRUE, width = NULL)),
                      column(10,sliderInput('ratioOff', 'Counts/Offset >=', min=1, max=5, value=1.5, step=0.1)),
                      column(2, checkboxInput('concFilcb', NULL, value = TRUE, width = NULL)),
                      column(10,sliderInput('concFil', 'Concentration [nM]', min=0, max=5000,  value=3000, step=100))
             ),
             tabPanel('Foc.Vol.',
                      tags$hr(),
                      fluidRow(column(5, textInput('voxel_XY_sixe_um', 'Voxel size XY (um)', 0)),
                               column(5, textInput('voxel_Z_sixe_um', 'Voxel size Z (um)', 0)),
                               column(2, checkboxInput('fixvolume', 'Use default',  value = FALSE))),
                      radioButtons("focaluse", label = h4('Focal volume'),
                                   choices = list("Use measured" = 1, "Use default" = 2),
                                   selected = 1),
                      fluidRow(column(6,checkboxInput('fixkappa', 'Fix kappa',  value = FALSE)),
                               column(6,textInput('kappaF', 'kappa', 0))),
                      fluidRow(column(4,
                                      textInput('w0Ch1', 'w0 Ch1 (um)', 0)),
                               column(4,
                                      textInput('w0Ch2', 'w0 Ch2 (um)', 0)),
                               column(4,
                                      textInput('w0Cross', 'w0 Chx (um)', 0))
                      ),
                      fluidRow(column(4,
                                      textInput('V0Ch1', 'Vol Ch1 (fl)', 0)),
                               column(4,
                                      textInput('V0Ch2', 'Vol Ch2 (fl)', 0)),
                               column(4,
                                      textInput('V0Cross', 'Vol Chx (fl)', 0))
                      ),
                      fluidRow(column(12,radioButtons("baselineuse", label = tags$h4('F.I. Baseline'),
                                                     choices = list("Measured" = 1, "User provided" = 2, "From file" = 3), selected = 1, inline = TRUE)),
                               column(4, textInput('baselU', 'Value', 0)),
                               column(8, textInput('baselfile', 'File name', '')))
             ) # tabpanel
           ) #tabset panel
    ), # main column
    column(8,
           tabsetPanel(

             tabPanel('Calibration curve',
                      fluidRow(column(12,
                                      plotOutput('CvsChint', height = 400,
                                                 dblclick = "plot_dblclick",brush = brushOpts(id = "plot_brush",resetOnNew = T)))),
                      fluidRow(column(6, tags$h4('R2'),
                                      verbatimTextOutput('R2qtile'),
                                      plotOutput('R2g', height = 100)),
                               column(6,
                                      tags$h4('Chi2'),
                                      verbatimTextOutput('Chi2qtile'),
                                      plotOutput('Chi2g', height = 100)))
             ),
             tabPanel('Other stats',

                      fluidRow(column(6, tags$h4('CPM'),verbatimTextOutput('CPMmeans'),
                                      plotOutput('CPMg',  height = 100)),
                               column(6, tags$h4('FCS Counts'),
                                      plotOutput('CountsvsFI',  height = 100))),
                      fluidRow(column(6, tags$h4('Ccorr [nM]'),verbatimTextOutput('CCorrmeans'),
                                      plotOutput('Ccorrg',  height = 100))),
                      fluidRow(column(5, selectInput('stataxis1', 'Axis_x', c('Interval1.Ch1'))),
                               column(5, selectInput('stataxis2', 'Axis_y', c('Interval2.Ch1')))),
                      fluidRow(plotOutput('statplot',  height = 200))
             )
           )

    )
  )
)
)


