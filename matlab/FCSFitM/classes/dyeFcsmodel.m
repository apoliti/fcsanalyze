classdef dyeFcsmodel < absFcsmodel
    % DYEFCSMODEL class to fit auto correlation function coming from dye measurement
    % a one component model specific for dye like blinking can be fitted
    % There are specific fitting workflows implemented.
    % fit is performed by 
    properties (Access = public)
        % these are the default parameters for a dye
        par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 7, 'f1', 1, 'tauD1', 20, 'alpha1', 1,    'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);  
        lb =    struct('N', 0.0001, 'thetaT', 0.001,'tauT', 0.1, 'f1', 0, 'tauD1', 1, 'alpha1', 0.5,  'tauD2', 500,   'alpha2', 0.5,  'kappa', 1);
        hb =    struct('N', 10000,  'thetaT', 1, 'tauT', 15,  'f1', 1,  'tauD1', 50000,'alpha1', 1.2, 'tauD2', 500000,  'alpha2', 2,  'kappa', 20);
        diffCoeff   = [464, 521]; %Alexe488 at 37 degrees,  Alexe568 at 37 degrees Malte Wachsmuth SPIM FCS measurement
        tauBoundary = [2, 10000]; %boundary of where to fit the data in us
        model = 1;
    end
    
    methods
        function MO = dyeFcsmodel(parnames, parvalues, lbvalues, hbvalues)
            % DYEFCSMODEL  constructor
            %   MO = dyeFcsmodel() - just create class with default parameter values and boundary values
            %   MO = dyeFcsmodel(parnames, parvalues) - create class and assign parvalues to parameters named parnames
            %   MO = dyeFcsmodel(parnames, parvalues, lbvalues)- create class and assign parvalues, lower boundary to parameter named in parnames.
            %   MO = dyeFcsmodel(parnames, parvalues, lbvalues,hbvalues) - create class and assign parvalues, lower and higher boundary to parameter named in parnames.
            %       parnames  -  cell array containing name of parameteres to be changed 
            %       parvalues -  vector with values of parameters same length than parnames
            %       lbvalues  -  vector with values of low boundary parameters same length than parnames
            %       hbvalues  -  vector with values of high boundary parameters same length than parnames
            
            % we cannot call MO.par in the constructor before call of super class this is the reason for the repetitions of the par values
            par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 7, 'f1', 1, 'tauD1', 20, 'alpha1', 1,    'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);  
            lb =    struct('N', 0.0001, 'thetaT', 0.05,'tauT', 0.1, 'f1', 1, 'tauD1', 1, 'alpha1', 0.5,  'tauD2', 500,   'alpha2', 0.5,  'kappa', 1);
            hb =    struct('N', 10000,  'thetaT', 1, 'tauT', 15,  'f1', 1,  'tauD1', 50000,'alpha1', 1.2, 'tauD2', 500000,  'alpha2', 2,  'kappa', 20);
            if nargin < 1
                parnames = fieldnames(par);
            end
            if nargin < 2
                parvalues = struct2array(par);
            end
            if nargin < 3
                lbvalues = struct2array(lb);
            end
            if nargin < 4
                hbvalues = struct2array(hb);
            end

            MO@absFcsmodel(parnames, parvalues, lbvalues, hbvalues)
                
        end
        
        function G = Gcomp(MO, par, tau)
            % GCOMP returns ACF for dye like blinking
            %   par - struct of parameter values
            %   tau - time points to evaluate function
            if ~isstruct(par)
                par = MO.parvecToparstruct(par);
            end
            triplet = (1 - par.thetaT + par.thetaT*exp(-tau/par.tauT))/(1-par.thetaT);
            comp1  = ((1+(tau/par.tauD1).^par.alpha1).^-1).*((1+1/par.kappa^2*(tau/par.tauD1).^par.alpha1).^-0.5);
            comp2 =  ((1+(tau/par.tauD2).^par.alpha2).^-1).*((1+1/par.kappa^2*(tau/par.tauD2).^par.alpha2).^-0.5);
            G = 1/par.N*triplet.*(par.f1*comp1 + (1-par.f1)*comp2);
        end

        function vol = computeVolume(MO, par, dyetype)
            % COMPUTEVOLUME Compute focal volume return value in femtoliter
            %   par  - vector or struct of parameters
            if isstruct(par)
                vol = pi^(3/2).*MO.computeRadius(par, dyetype).^3.*par.kappa;
            else
                vol = pi^(3/2).*MO.computeRadius(par, dyetype).^3.*par(:,9);
            end
        end
        
        function vol = computeConcentration(MO, par, dyetype)
            % COMPUTECONCENTRATION Compute concentration and returns value
            % in nM
            %   par  - vector or struct of parameters
            % Na = 0.6022140 (=
            % 6.022140*10^23*10^-15(femtoliter)*10^-9(to get nM))
            Na = 0.6022140;
            if isstruct(par)
                vol = par.N./MO.computeVolume(par, dyetype)/Na; 
            else
                vol = par(:,1)./MO.computeVolume(par, dyetype)/Na;
            end
        end
        
        function w0 = computeRadius(MO, par, dyetype)
            % COMPUTERADIUS Compute focal radius and return value in um
            %   par  - vector or struct of parameters
            if isstruct(par)
                w0 = 2*sqrt(MO.diffCoeff(dyetype)*par.tauD1)/1000;
            else
                w0 = 2*sqrt(MO.diffCoeff(dyetype)*par(:,5))/1000;
            end
        end
      
        function [outPar, norm] = fitThetaTtauT(MO, data, kappa)
            % FITTHETATTAUT fit N, tauD1, thetaT and tauT for a fixed kappa value
            %   INPUT:
            %       data   - correlation data [tau, corr]
            %       kappa  - kappa value will be kept fixed. Typically at 5 (this is more stable than allowing for verying thetaT and kappa simultaneously)
            %   OUTPUT:
            %       outPar - 'N', 'tauD1', 'thetaT', 'tauT'
            %       norm   - norm at optimum
            
            if nargin < 3
                kappa = 5;
            end
            %first find optimal tauT
            pari = MO.par;
            pari.kappa = kappa;
            
            %find initial value for tauD1 and N repeat 5 times
            for itry = 1:MO.maxfittry
                [N, tauD1, tauD2] = MO.initialGuess(data, MO.tauBoundary);
                [parf{itry}, normT(itry)] = MO.fitmodel([N, tauD1], {'N', 'tauD1'}, pari, data, MO.tauBoundary);
            end
            [val, pos] = min(normT);
            parf1 = parf{pos};
            
            %fit thetaT and tauT for a fixed kappa
            for itry = 1:MO.maxfittry
                [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1,  pari.thetaT+(1-2*rand)*pari.thetaT/8, ...
                    pari.tauT+(1-2*rand)*pari.tauT/8], {'N', 'tauD1', 'thetaT', 'tauT'}, parf1, data,  MO.tauBoundary);
            end
            [val, pos] = min(normT);
            parf2 = parf{pos};
            outPar(1,:) = struct2array(parf2);
            norm(1,1) = normT(pos);
        end
        
        function [thetaT, tauT] = fitThetaTtauTArray(MO, dataC, kappa, iC)
            % FITTHETATTAUTARRAY find optimal thetaT and tauT for several data simultaneously with fixed kappa
            %   INPUT:
            %       dataC  - a cell array containing [tau corrCh1 corrCh1_e corrCh2 corrCh2_e Xcorr Xcorr_e] arrays
            %       kappa  - fixed kappa value
            %       iC     - index of channel to be used 1 - Ch1, 2 - Ch2, 3 - XCorr 
            %   OUTPUT:
            %       thetaT - mean optimal thetaT for several measurements
            %       tauT   - mean optimal tauT for several measurements
            
            if ~iscell(dataC)
                dataC = {dataC};
                warning('fitThetaTtauTArray: data is not a cell array');
            end
            outParCorr = [];
            outParCorr_thetaT = [];
            for idata = 1:length(dataC)
                data = dataC{idata}(:,[1 iC*2 iC*2+1]);
                if data(1,2) > 0
                    [thetTfit(idata,:), normCorr_thetaT(:,idata)] = MO.fitThetaTtauT(data, kappa);
                else
                    thetTfit(idata,3) = 1; 
                    thetTfit(idata,2) = 1;
                end
            end
            tauT = mean(thetTfit(:,3));
            thetaT = mean(thetTfit(:,2));
        end
        
        function [outPar, norm] = fitKappaVal(MO, data, kappaVal)
            % FITKAPPAVAL compute N and tauD1 for different values of kappa. All other parameters remain constant
            %   data     - an array [tau, corr]
            %   kappaVal - a vector containing values for kappa
            
            for i = 1:length(kappaVal)
                pari = MO.par;
                pari.kappa = kappaVal(i);
                clear('parf')
                for itry = 1:MO.maxfittry
                    [N, tauD1, tauD2] = MO.initialGuess(data, 2, 10000);
                    [parf{itry}, normT(itry)] = MO.fitmodel([N, tauD1], {'N', 'tauD1'}, pari, data, MO.tauBoundary);
                end
                
                [val, pos] = min(normT);
                parf1 = parf{pos};
                [parf, norm(i,1)] = MO.fitmodel([parf1.N, parf1.tauD1], {'N', 'tauD1'}, parf1, data, MO.tauBoundary);
                outPar(i,:) = struct2array(parf);
            end
        end
        
        function [outPar, norm] = fitKappaValThetaT(MO, data, kappaVal)
            % FITKAPPAVALTHETAT compute N and tauD1 for different values of kappa. All other parameters remain constant
            %   INPUT:
            %       data     - an array [tau, corr]
            %       kappaVal - a vector containing values for kappa
            %   OUTPUT:
            %       outPar   - N, tauD1, thetaT
            %       norm     - norm of fit
            
            for i = 1:length(kappaVal)
                
                pari = MO.par;
                pari.kappa = kappaVal(i);
                %display(sprintf('Run fit for kappa %.1f ',kappaVal(i)));
                % get initial guess for N and tauD1
                for itry = 1:MO.maxfittry
                    [N, tauD1, tauD2] = MO.initialGuess(data, MO.tauBoundary);
                    [parf{itry}, normT(itry)] = MO.fitmodel([N, tauD1], {'N', 'tauD1'}, pari, data, MO.tauBoundary);
                end
                [val, pos] = min(normT);
                parf1 = parf{pos};
                %fit N, tauD1 and thetaT using initial guess of N and tauD1, tauR remains fixed for all measurements
                for itry = 1:MO.maxfittry
                    [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1,  pari.thetaT+(1-2*rand)*pari.thetaT/10 ], ...
                        {'N', 'tauD1', 'thetaT'}, parf1, data, MO.tauBoundary);
                end
                [val, pos] = min(normT);
                parf2 = parf{pos};
                norm(i,1) = normT(pos);
                outPar(i,:) = struct2array(parf2);
            end
        end
    end

end

