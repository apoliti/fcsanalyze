classdef xmlFA
    % XMLFA class that contains static method to access and write xml files
    % generated from Fluctualizer, Malte Wachsmuth Software
    % Note that java is base 0, matlab is base 1
    methods(Static)
        function docNode = readnode(fname)
            docNode = xmlread(fname);
        end
        
        
        function  ann = readannotation(fname)
            % READANNOTATION reads an xml document fname and returns annotation
            % ann = character ASCII code!!
            
            docNode = xmlread(fname);
            nodes = docNode.getElementsByTagName('U8');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_nAnnotation'
                        ann = char(node.item(3).getTextContent);
                        break;
                end
            end
        end
        
        function docNode = writeannotation(docNode, ann)
            % ann is ASCII chracter code
            nodes = docNode.getElementsByTagName('U8');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_nAnnotation'
                        if isempty(ann)
                            node.item(3).setTextContent('32');
                            break
                        else
                            if ischar(ann)
                                node.item(3).setTextContent(num2str(double(ann)));
                            else
                                node.item(3).setTextContent(num2str(ann));
                            end
                            break;
                        end
                end
            end
        end
        
        function par = readpar(fname)
            % REAEDPAR reads an xml document fname and returns fitting parameter
            % par = [par1_ch1 par2_ch1 ...; par1_ch2 ...; par1_xc ....]
            %       [N thetaT tauT f1 tauD1 alpha1 tauD2 alpha2 kappa; ....]
            
            docNode = xmlread(fname);
            nodes = docNode.getElementsByTagName('Array_DBL');
            par = zeros(3,25);
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_rFitParaValue1'
                        par(1,:) = read_xml_array(node);
                    case 'm_rFitParaValue2'
                        par(2,:) = read_xml_array(node);
                    case 'm_rFitParaValueX'
                        par(3,:) = read_xml_array(node);
                        break;
                end
            end
            par = par(:,1:9);
        end
        
        function docNode = writepar(docNode, par, lb, hb)
            % WRITEPAR write model parameter to xml file
            % These are 9 parameters base of jave is 0
            nodes = docNode.getElementsByTagName('Array_DBL');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    
                    case {'m_rFitParaLower1', 'm_rFitParaLower2', 'm_rFitParaLowerX'}
                        nodeVal = node.getElementsByTagName('Val');
                        for j = 0:8
                            nodeVal.item(j).setTextContent(num2str(lb(1,j+1)));
                        end
                    case {'m_rFitParaUpper1','m_rFitParaUpper2', 'm_rFitParaUpperX'}
                        nodeVal = node.getElementsByTagName('Val');
                        for j = 0:8
                            nodeVal.item(j).setTextContent(num2str(hb(1,j+1)));
                        end
                    case 'm_rFitParaValue1'
                        nodeVal = node.getElementsByTagName('Val');
                        for j = 0:8
                            nodeVal.item(j).setTextContent(num2str(par(1,j+1)));
                        end
                    case 'm_rFitParaValue2'
                        if size(par,1) > 1
                            nodeVal = node.getElementsByTagName('Val');
                            for j = 0:8
                                nodeVal.item(j).setTextContent(num2str(par(2,j+1)));
                            end
                        end
                    case 'm_rFitParaValueX'
                        if size(par,1) > 2
                            nodeVal = node.getElementsByTagName('Val');
                            for j = 0:8
                                nodeVal.item(j).setTextContent(num2str(par(3,j+1)));
                            end
                        end
                        
                end
            end
            nodes = docNode.getElementsByTagName('Array_I32');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case {'m_bFitParaFixed1', 'm_bFitParaFixed2', 'm_bFitParaFixedX'}
                        nodeVal = node.getElementsByTagName('Val');
                        for j = 0:8
                            nodeVal.item(j).setTextContent(num2str(1));
                        end
                end
            end
        end
        
        function [fit, idx] = readfit(fname)
            %READFIT read fit from all channels
            % INPUT:
            %   fname - name of xml file
            %   output - [tau_us fitCh1 fitCh2 fitXC]
            docNode = xmlread(fname);
            nodes = docNode.getElementsByTagName('Array_DBL');
            %% get nodes with Array_DBL (these are the entries for ACfits)
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_rCorrelationLagTime'
                        tau = read_xml_array(node);
                        maxV = find(tau==0);
                        if isempty(maxV)
                            maxV = length(tau);
                        else
                            maxV = maxV(1)-1;
                        end
                    case 'm_rFitCorrelation1'
                        fit1 = read_xml_array(node);
                    case  'm_rFitCorrelation2'
                        fit2 = read_xml_array(node);
                    case  'm_rFitCorrelationX'
                        fitX = read_xml_array(node);
                        break;
                end
            end
            nodes = docNode.getElementsByTagName('U32');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_nFitFirst'
                        idx(1) = str2num(node.item(3).getTextContent);
                    case 'm_nFitSecond'
                        idx(2) = str2num(node.item(3).getTextContent);
                        break;
                end
            end
            % this does not account for start
            fit = [tau' fit1' fit2' fitX'];
            fit = fit(idx(1)+1:maxV-idx(2),:);
            
            
        end
        
        function docNode = writefit(docNode, fit, idxs, model)
            %WRITEFIT write fit for of all channels
            %   docNode - xml document node
            %   fit     - a [N_tau 3] array containig fits for Ch1, Ch2 and XC
            nodes = docNode.getElementsByTagName('Array_DBL');
            %m_nFitFirst start index base 0 of fit
            %m_nFitSecond end index of fit end-m_nFitSecond. 0 is complete data set
            
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    
                    case 'm_rFitCorrelation1'
                        nodeVal = node.getElementsByTagName('Val');
                        for j = idxs(1)-1:length(fit)+idxs(1)-2
                            nodeVal.item(j).setTextContent(num2str(fit(j+2-idxs(1),1)));
                        end
                        
                    case 'm_rFitCorrelation2'
                        if size(fit, 2) > 1 && sum(fit(:,2)) ~= 0
                            nodeVal = node.getElementsByTagName('Val');
                            for j = idxs(1)-1:length(fit)+idxs(1)-2
                                nodeVal.item(j).setTextContent(num2str(fit(j+2-idxs(1),2)));
                            end
                        end
                        
                    case 'm_rFitCorrelationX'
                        if size(fit, 2) > 2 && sum(fit(:,3)) ~= 0
                            nodeVal = node.getElementsByTagName('Val');
                            for j = idxs(1)-1:length(fit)+idxs(1)-2
                                nodeVal.item(j).setTextContent(num2str(fit(j+2-idxs(1),3)));
                            end
                        end
                end
            end
            nodes = docNode.getElementsByTagName('U32');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_nFitFirst'
                        node.item(3).setTextContent(num2str(idxs(1)-1));
                    case 'm_nFitSecond'
                        node.item(3).setTextContent(num2str(idxs(2)));
                    case 'm_nFitModelNo'
                        node.item(3).setTextContent(num2str(model));
                        
                end
            end
            nodes = docNode.getElementsByTagName('I32');
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_bFitted1'
                        if sum(fit(:,1)) ~= 0
                            nodeVal = node.getElementsByTagName('Val');
                            nodeVal.item(0).setTextContent(num2str(1));
                        end
                    case 'm_bFitted2'
                        if size(fit,2) > 1 && sum(fit(:,2)) ~= 0
                            nodeVal = node.getElementsByTagName('Val');
                            nodeVal.item(0).setTextContent(num2str(1));
                        end
                    case  'm_bFittedX'
                        if size(fit,2) > 2 && sum(fit(:,3)) ~= 0
                            nodeVal = node.getElementsByTagName('Val');
                            nodeVal.item(0).setTextContent(num2str(1));
                        end
                end
            end
            
        end
        
        function writenode(fname, docNode)
            xmlwriteFA(fname, docNode);
        end
        
        
        function cor = readcor(fname)
            % READCOR read an xml document and return correlation data. Cor is array dim = [N_time 7]
            % cor = [tau AC1 AC1_error AC2 AC2_error XC XC_error]
            % Note that error from FA must be rescaled by 1/sqrt(20). Due to a window of 20
            % This step is done in the workflow_fit_protein or dye
            docNode = xmlread(fname);
            nodes = docNode.getElementsByTagName('Array_DBL');
            %% get nodes with Array_DBL (these are the entries for AC and ACfits)
            for i=0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_rCorrelationLagTime'
                        tau = read_xml_array(node);
                        maxV = find(tau==0);
                        if isempty(maxV)
                            maxV = length(tau);
                        else
                            maxV = maxV(1)-1;
                        end
                    case  'm_rCorrelationValue1'
                        AC1 = read_xml_array(node);
                    case  'm_rCorrelationValue2'
                        AC2 = read_xml_array(node);
                    case  'm_rCorrelationValueX'
                        XC = read_xml_array(node);
                    case  'm_rCorrelationError1'
                        AC1_e = read_xml_array(node);
                    case  'm_rCorrelationError2'
                        AC2_e = read_xml_array(node);
                    case  'm_rCorrelationErrorX'
                        XC_e = read_xml_array(node);
                        break;
                end
            end
            cor = [tau' AC1' AC1_e' AC2' AC2_e' XC' XC_e'];
            cor = cor(1:maxV, :);
        end
        
        function ic = readic(fname)
            % READIC read an xml document and return intensity counts data. ic is array dim = [N_time 3]
            % ic = [time ic1 ic2]
            % time: time in sec
            % ic1: photon counts kHz channel1
            % ic2: photon counts kHz channel2
            docNode = xmlread(fname);
            nodes = docNode.getElementsByTagName('Array_SGL');
            %% get nodes with Array_DBL (these are the entries for AC and ACfits)
            for i = 0:nodes.getLength-1
                node = nodes.item(i);
                switch(char(node.item(1).getTextContent))
                    case 'm_rDisplayTime'
                        time = read_xml_array(node);
                    case  'm_rDisplayTrace1'
                        ic1 = read_xml_array(node);
                    case  'm_rDisplayTrace2'
                        ic2 = read_xml_array(node);
                        break;
                end
            end
            ic = [time' ic1' ic2'];
        end
        
    end
end

function xmlwriteFA(fname, docNode)
xmlwrite(fname, docNode); %CR and LF darstellung probleme
% TODO Annotation becomes NaN should be a free space instead
end

function  arrayO = read_xml_array(node, idx)
% READ_XML_ARRAY read an array as given by FA Array_DBL, e.g.
%<Array_DBL>
%<Name>m_rCorrelationLagTime</Name>
%<Dimsize>250</Dimsize>
%<Val>1.00000000000000</Val>
if nargin < 2
    %first 3 entries are not the values of array
    idx = 4;
end
arrayO = strsplit(char(node.getTextContent));
arrayO = str2double(arrayO(idx:end-1));
end

