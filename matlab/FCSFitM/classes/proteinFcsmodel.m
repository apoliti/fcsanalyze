classdef proteinFcsmodel < absFcsmodel
    % PROTEINFCSMODEL class to fit auto correlation of protein
    % The workflow is a one component fit and a 2 component fit of 1 or 2
    % channels
    
    properties (Access = public)
        % these are the default parameters for a protein
        par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 100, 'f1', 1, 'tauD1', 500, 'alpha1', 1,    'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);
        %par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 2, 'f1', 1, 'tauD1', 500, 'alpha1', 1,    'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);
        lb =    struct('N', 0.0001, 'thetaT', 0.001,'tauT', 0, 'f1', 0, 'tauD1', 100, 'alpha1', 0.5,  'tauD2', 500,   'alpha2', 0.5,  'kappa', 1);
        hb =    struct('N', 100000,  'thetaT', 1, 'tauT', 1000,  'f1', 1,  'tauD1', 50000,'alpha1', 1.2, 'tauD2', 5*10^6,  'alpha2', 2,  'kappa', 20);
        diffCoeff   = [464, 521]; %Alexe488 at 37 degrees,  Alexe568 at 37 degrees Malte Wachsmuth SPIM FCS measurement
        tauBoundary = []; %boundary of where to fit the data if empty fit the range
        model = 0;
    end
    
    methods
        function MO = proteinFcsmodel(parnames, parvalues, lbvalues, hbvalues)
            % PROTEINFCSMODEL  constructor
            %   MO = proteinFcsmodel() - just create class with default parameter values and boundary values
            %   MO = proteinFcsmodel(parnames, parvalues) - create class and assign parvalues to parameters named parnames
            %   MO = proteinFcsmodel(parnames, parvalues, lbvalues)- create class and assign parvalues, lower boundary to parameter named in parnames.
            %   MO = proteinFcsmodel(parnames, parvalues, lbvalues,hbvalues) - create class and assign parvalues, lower and higher boundary to parameter named in parnames.
            %       parnames  -  cell array containing name of parameteres to be changed
            %       parvalues -  vector with values of parameters same length than parnames
            %       lbvalues  -  vector with values of low boundary parameters same length than parnames
            %       hbvalues  -  vector with values of high boundary parameters same length than parnames
            
            % we cannot call MO.par in the constructor before call of super class this is the reason for the repetitions of the par values
            par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 100, 'f1', 1, 'tauD1', 500, 'alpha1', 1,    'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);
            lb =    struct('N', 0.0001, 'thetaT', 0.001,'tauT', 0, 'f1', 0, 'tauD1', 100, 'alpha1', 0.5,  'tauD2', 500,   'alpha2', 0.5,  'kappa', 1);
            hb =    struct('N', 100000,  'thetaT', 1, 'tauT', 1000,  'f1', 1,  'tauD1', 50000,'alpha1', 1.2, 'tauD2', 5*10^6,  'alpha2', 2,  'kappa', 20);
            if nargin < 1
                parnames = fieldnames(par);
            end
            if nargin < 2
                parvalues = struct2array(par);
            end
            if nargin < 3
                lbvalues = struct2array(lb);
            end
            if nargin < 4
                hbvalues = struct2array(hb);
            end
            
            MO@absFcsmodel(parnames, parvalues, lbvalues, hbvalues)
            
        end
        
        function G = Gcomp(MO, par, tau)
            % GCOMP returns ACF for dye like blinking
            %   par - struct or full vector of parameter values
            %   tau - time points to evaluate function
            if ~isstruct(par)
                par = MO.parvecToparstruct(par);
            end
            triplet = (1 - par.thetaT + par.thetaT*exp(-tau/par.tauT));
            comp1  = ((1+(tau/par.tauD1).^par.alpha1).^-1).*((1+1/par.kappa^2*(tau/par.tauD1).^par.alpha1).^-0.5);
            comp2 =  ((1+(tau/par.tauD2).^par.alpha2).^-1).*((1+1/par.kappa^2*(tau/par.tauD2).^par.alpha2).^-0.5);
            G = 1/par.N*triplet.*(par.f1*comp1 + (1-par.f1)*comp2);
        end
    end
end

