function varargout = FCSFitM(varargin)
% FCSFITM MATLAB code for FCSFitM.fig
%      FCSFITM, by itself, creates a new FCSFITM or raises the existing
%      singleton*.
%
%      H = FCSFITM returns the handle to a new FCSFITM or the handle to
%      the existing singleton*.
%
%      FCSFITM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FCSFITM.M with the given input arguments.
%
%      FCSFITM('Property','Value',...) creates a new FCSFITM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FCSFitM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FCSFitM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FCSFitM

% Last Modified by GUIDE v2.5 22-Jun-2017 13:57:21

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @FCSFitM_OpeningFcn, ...
    'gui_OutputFcn',  @FCSFitM_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
mfile = mfilename('fullpath');
genpath_loc(fileparts(mfile));
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FCSFitM is made visible.
function FCSFitM_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FCSFitM (see VARARGIN)

% Choose default command line output for FCSFitM
handles.output = hObject;

% delete any left-over waitbar windows
delete(findall(0,'Tag','TMWWaitbar'))
% add required files
addpath(genpath(fileparts(mfilename('fullpath'))), '-end');
% variable to remember the last-path used
handles.lastpath = '';
% Create a progressbar
handles.progressbar = uiProgressBar('fighandle', handles.statusbar);

% focal volume file global
handles.focVolfile_global = '';

% Set the colors indicating a selected/unselected tab
handles.unselectedTabColor=get(handles.tab1text,'BackgroundColor');
handles.selectedTabColor=handles.unselectedTabColor-0.1;
% tab figure
% Set units to normalize for easier handling
set(handles.tab1text,'Units','normalized')
set(handles.tab2text,'Units','normalized')
set(handles.tab3text,'Units','normalized')
set(handles.tab1panel,'Units','normalized')
set(handles.tab2panel,'Units','normalized')
set(handles.tab3panel,'Units','normalized')

% Tab 1
tabnames = {'FCS background', 'Fit dye eff. volume', 'Fit protein/dye'};

for itab = 1:3
    pos{itab} = get(getfield(handles,['tab' num2str(itab) 'text']),'Position');
    if itab == 1
        post = [(pos{itab}(3)-pos{itab}(1))/2, pos{itab}(2)/2+pos{itab}(4)];
        tabcolor = handles.selectedTabColor;
    else
        pos{itab}(1) = pos{itab-1}(1)+pos{itab-1}(3);
        post = [pos{itab}(3)/2, pos{itab}(2)/2+pos{itab}(4)];
        tabcolor = handles.unselectedTabColor;
    end
    handles = setfield(handles, ['a' num2str(itab)], axes('Units','normalized',...
        'Box','on',...
        'XTick',[],...
        'YTick',[],...
        'Color',tabcolor,...
        'Position',[pos{itab}(1)*1.1 pos{itab}(2) pos{itab}(3)*1.1 pos{itab}(4)+0.01],...
        'ButtonDownFcn',sprintf('FCSFitM(''a%dbd'',gcbo,[],guidata(gcbo))', itab)));
    
    
    handles = setfield(handles, ['t'  num2str(itab)], text('String',tabnames{itab},...
        'Units','normalized',...
        'Position',post,...
        'HorizontalAlignment','left',...
        'VerticalAlignment','middle',...
        'Margin',0.001,...
        'FontSize',12,...
        'Backgroundcolor',tabcolor,...
        'ButtonDownFcn',sprintf('FCSFitM(''t%dbd'',gcbo,[],guidata(gcbo))', itab)));
    
end


% Manage panels (place them in the correct position and manage visibilities)
pan1pos=get(handles.tab1panel,'Position');
set(handles.tab2panel,'Position',pan1pos);
set(handles.tab3panel,'Position',pan1pos);
set(handles.tab2panel,'Visible','off');
set(handles.tab3panel,'Visible','off');
set(handles.versiontext, 'String', 'v0.9');
handles.oldPos = get(handles.figure1, 'position');
fs = get(handles.popup_fontsize, 'String');
fs = fs{get(handles.popup_fontsize, 'Value')};

set(findall(handles.figure1,'-property','FontSize'),'FontSize',str2num(fs))
guidata(hObject, handles);
% UIWAIT makes FCSFitM wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = FCSFitM_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% Text object 1 callback (tab 1)
function t1bd(hObject,eventdata,handles)
set(hObject,'BackgroundColor',handles.selectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)
set(handles.t3,'BackgroundColor',handles.unselectedTabColor)
set(handles.a1,'Color',handles.selectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)
set(handles.a3,'Color',handles.unselectedTabColor)
set(handles.tab1panel,'Visible','on')
set(handles.tab2panel,'Visible','off')
set(handles.tab3panel,'Visible','off')


% Text object 2 callback (tab 2)
function t2bd(hObject,eventdata,handles)
set(hObject,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.t3,'BackgroundColor',handles.unselectedTabColor)
set(handles.a2,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.a3,'Color',handles.unselectedTabColor)
set(handles.tab2panel,'Visible','on')
set(handles.tab1panel,'Visible','off')
set(handles.tab3panel,'Visible','off')


% Text object 3 callback (tab 3)
function t3bd(hObject,eventdata,handles)
set(hObject,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)
set(handles.a3,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)
set(handles.tab3panel,'Visible','on')
set(handles.tab1panel,'Visible','off')
set(handles.tab2panel,'Visible','off')


% Axes object 1 callback (tab 1)
function a1bd(hObject,eventdata,handles)

set(hObject,'Color',handles.selectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)
set(handles.a3,'Color',handles.unselectedTabColor)
set(handles.t1,'BackgroundColor',handles.selectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)
set(handles.t3,'BackgroundColor',handles.unselectedTabColor)
set(handles.tab1panel,'Visible','on')
set(handles.tab2panel,'Visible','off')
set(handles.tab3panel,'Visible','off')


% Axes object 2 callback (tab 2)
function a2bd(hObject,eventdata,handles)

set(hObject,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.a3,'Color',handles.unselectedTabColor)
set(handles.t2,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.t3,'BackgroundColor',handles.unselectedTabColor)
set(handles.tab2panel,'Visible','on')
set(handles.tab1panel,'Visible','off')
set(handles.tab3panel,'Visible','off')


% Axes object 3 callback (tab 3)
function a3bd(hObject,eventdata,handles)

set(hObject,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)
set(handles.t3,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)
set(handles.tab3panel,'Visible','on')
set(handles.tab1panel,'Visible','off')
set(handles.tab2panel,'Visible','off')

% --- Executes on button press in getdir_bg.
function getdir_bg_Callback(hObject, eventdata, handles)
% hObject    handle to getdir_bg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pathname = uigetdir(handles.lastpath, 'Select directory of data for FCS background counts');
if pathname
    set(handles.bg_pathtext, 'String', pathname);
    handles.lastpath = pathname;
    guidata(hObject, handles);
    fnames = getAllFiles(pathname, 'fcs');
    set(handles.bg_listbox, 'string', fnames);
end


% --- Executes on button press in getdir_fitdye.
function getdir_fitdye_Callback(hObject, eventdata, handles)
% hObject    handle to getdir_fitdye (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pathname = uigetdir(handles.lastpath, 'Select directory of data for fluorescent dye')
if pathname
    set(handles.dye_pathtext, 'String', pathname);
    handles.lastpath = pathname;
    guidata(hObject, handles);
    FAsession = get(handles.dye_session, 'String');
    % do not process directories recursively
    processChange(handles, 1);
    xmlFAfiles = getAllFiles(pathname, 'xml',['\.' FAsession '\.'], 0);
    zenfiles   = getAllFiles(pathname, 'fcs', [], 0);
    if isempty(xmlFAfiles)
        if isempty(zenfiles)
            warndlg(sprintf('%s\n%s', 'No FA xml files nor .fcs files have been found.', 'Check the content of the directory!'));
            set(handles.dye_listbox, 'string', {});
        else
            warndlg('No FA files found. Use .fcs files instead');
            
            set(handles.dye_listbox, 'string', zenfiles);
            return
        end
    else
        set(handles.dye_listbox, 'string', xmlFAfiles);
    end
    processChange(handles, 0);
end


% --- Executes on selection change in bg_listbox.
function bg_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to bg_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bg_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bg_listbox


% --- Executes during object creation, after setting all properties.
function bg_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bg_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






function bg_counts_Ch1_Callback(hObject, eventdata, handles)
% hObject    handle to bg_counts_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bg_counts_Ch1 as text
%        str2double(get(hObject,'String')) returns contents of bg_counts_Ch1 as a double


% --- Executes during object creation, after setting all properties.
function bg_counts_Ch1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bg_counts_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bg_counts_Ch2_Callback(hObject, eventdata, handles)
% hObject    handle to bg_counts_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bg_counts_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of bg_counts_Ch2 as a double


% --- Executes during object creation, after setting all properties.
function bg_counts_Ch2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bg_counts_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function processChange(handles, status)
if status == 1
    set(handles.process_textbox, 'String', 'Processing ...');
    set(handles.process_textbox, 'Backgroundcolor', 'white');
else
    set(handles.process_textbox, 'String', 'Ready');
    set(handles.process_textbox, 'Backgroundcolor', 'white');
end


% --- Executes on button press in getcounts.
function getcounts_Callback(hObject, eventdata, handles)
% hObject    handle to getcounts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MO = proteinFcsmodel();
fnames = get(handles.bg_listbox, 'String');
selectedItems = get(handles.bg_listbox, 'value');
fnames_sel = fnames(selectedItems);
set(handles.getcounts, 'Enable', 'off');
set(handles.process_textbox, 'String', 'Processing ...');
handles.progressbar = uiProgressBar('axeshandle', handles.progressbar, 'fighandle', handles.statusbar, 'progress', 0.0);
pause(0.5)
drawnow()
mCounts = [];
for i=1:length(fnames_sel)
    [AC, mCountsl] = MO.readZeissfcs(fnames_sel{i});
    mCounts = [mCounts; mCountsl];
    uiProgressBar('axeshandle', handles.progressbar, 'fighandle', ...
        handles.statusbar, 'progress', i/length(fnames_sel));
    pause(0.5)
end

set(handles.getcounts, 'Enable', 'on');
handles.progressbar = uiProgressBar('axeshandle', handles.progressbar, 'fighandle', handles.statusbar, 'progress', 0.0);
set(handles.process_textbox, 'String', '');

set(handles.bg_counts_Ch1, 'String', num2str(mean(mCounts(:,1))));
set(handles.bg_counts_Ch2, 'String', num2str(mean(mCounts(:,2))));
drawnow()
pause(0.5)

% --- Executes on button press in dye_fit.
function dye_fit_Callback(hObject, eventdata, handles)
% hObject    handle to dye_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fnames = get(handles.dye_listbox, 'String');
if length(fnames) == 0
    warndlg('Specify directory that contains .fcs or .xml data for the fluorescent dye!')
    return
end
selectedItems = get(handles.dye_listbox, 'value');

fnames_sel = fnames(selectedItems);
set(handles.dye_abort, 'Enable', 'on');
set(handles.dye_abort, 'Value', 0);
pause(0.5);
handles.progressbar = uiProgressBar('fighandle', handles.statusbar, 'progress', 0.0);
drawnow();
if length(fnames_sel)>0
    [pathname, name, ext] = fileparts(fnames_sel{1});
    if ext == '.xml'
        xmlFAfiles = fnames_sel;
        zenfiles = {};
    elseif ext == '.fcs'
        xmlFAfiles = {};
        zenfiles = fnames_sel;
    else
        warndlg('File format is not FA xml or fcs,')
        return
    end
    force = get(handles.dye_force, 'Value');
    DCh1 = str2num(get(handles.dye_D_Ch1, 'String'));
    DCh2 = str2num(get(handles.dye_D_Ch2, 'String'));
    indir = get(handles.dye_pathtext, 'String');
    set(handles.figure1, 'pointer', 'watch');
    set(handles.process_textbox, 'String', 'Processing dye ...')
    drawnow();
    pause(0.5);
    try
        [focVolA, focVolfile, ChUsed] = workflow_fit_dye(indir, xmlFAfiles, zenfiles,  fullfile(indir, 'optimisedFit'), ...
            'kappaVal', [2:0.1:9]', 'force', force, 'diffCoeff', [DCh1, DCh2], ...
        'aborthandle', handles.dye_abort, 'progresshandle', handles.progressbar, 'weight', get(handles.dye_weight, 'Value'));
        
        setvolumegui(handles, focVolA);
        [pathname, fovolname, ext] =  fileparts(focVolfile);
        [pathname, lastpath] = fileparts(indir);
        
        set(handles.focalvolume_txt, 'String', ...
            sprintf('Focal volume data in %s\nFit results in %s ', fullfile(lastpath,[fovolname ext]), fullfile(lastpath, 'optimisedFit', '')));
        handles.focVolfile_global = focVolfile;
    catch err
        setvolumegui(handles);
        handles.focVolfile_global = '';
        set(handles.focalvolume_txt, 'String', '');
        if ~strcmp(err.identifier, 'workflow_fit_dye:abort')
            getReport(err)
        end
    end
    set(handles.figure1, 'pointer', 'arrow');
    set(handles.dye_abort, 'Value', 0);
    set(handles.dye_abort, 'Enable', 'on');
    set(handles.process_textbox, 'String', '')
    handles.progressbar = uiProgressBar('fighandle', handles.statusbar);
    guidata(hObject, handles);
    pause(0.5);
    drawnow();
end


function setvolumegui(handles, focVol)
handleVec = [handles.w0_Ch1, handles.w0_Ch2, handles.w0_ChX, handles.Veff_Ch1, ...
    handles.Veff_Ch2, handles.Veff_ChX, handles.kappa_Ch1, handles.kappa_Ch2, handles.kappa_ChX];

if nargin < 2
    for i=1:length(handleVec)
        set(handleVec(i), 'String', '');
    end
    return
end
for i=1:length(handleVec)
    set(handleVec(i), 'String', num2str(focVol(i)));
end


% --- Executes on selection change in dye_listbox.
function dye_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to dye_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dye_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dye_listbox

% --- Executes on selection change in dye_listbox.
function protein_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to dye_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dye_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dye_listbox

% --- Executes during object creation, after setting all properties.
function dye_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dye_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function temp_slider_Callback(hObject, eventdata, handles)
 sliderValue = get(handles.temp_slider,'Value');
 set(hObject, 'Value', round(sliderValue, 1));
 sliderValue = get(handles.temp_slider,'Value');
 set(handles.temp_text,'String',num2str(sliderValue));
 set(handles.dye_D_Ch1, 'String', num2str(round(computeDiffusion_coeff(sliderValue, 365, 27))));
 set(handles.dye_D_Ch2, 'String', num2str(round(computeDiffusion_coeff(sliderValue, 410, 27))));
 

function dye_D_Ch1_Callback(hObject, eventdata, handles)
% hObject    handle to dye_D_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dye_D_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of dye_D_Ch2 as a double



% --- Executes during object creation, after setting all properties.
function dye_D_Ch1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dye_D_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dye_D_Ch2_Callback(hObject, eventdata, handles)
% hObject    handle to dye_D_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dye_D_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of dye_D_Ch2 as a double


% --- Executes during object creation, after setting all properties.
function dye_D_Ch2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dye_D_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in bg_selectall.
function bg_selectall_Callback(hObject, eventdata, handles)
% hObject    handle to bg_selectall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fnames = get(handles.bg_listbox, 'String');
set(handles.bg_listbox, 'value', [1:length(fnames)]);


% --- Executes on button press in dye_selectall.
function dye_selectall_Callback(hObject, eventdata, handles)
% hObject    handle to dye_selectall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fnames = get(handles.dye_listbox, 'String');
set(handles.dye_listbox, 'value', [1:length(fnames)]);


function dye_session_Callback(hObject, eventdata, handles)
% hObject    handle to dye_session (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dye_session as text
%        str2double(get(hObject,'String')) returns contents of dye_session as a double


% --- Executes during object creation, after setting all properties.
function dye_session_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dye_session (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in dye_force.
function dye_force_Callback(hObject, eventdata, handles)
% hObject    handle to dye_force (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of dye_force


% --- Executes on button press in protein_force.
function protein_force_Callback(hObject, eventdata, handles)
% hObject    handle to dye_force (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of protein_force

% --- Executes on button press in protein_focvol
function protein_focvol_Callback(hObject, eventdata, handles)
% hObject    handle to dye_force (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in dye_abort.
function dye_abort_Callback(hObject, eventdata, handles)
% hObject    handle to dye_abort (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of dye_abort
drawnow()



function kappa_Ch2_Callback(hObject, eventdata, handles)
% hObject    handle to kappa_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kappa_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of kappa_Ch2 as a double


% --- Executes during object creation, after setting all properties.
function kappa_Ch2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kappa_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function w0_Ch2_Callback(hObject, eventdata, handles)
% hObject    handle to w0_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of w0_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of w0_Ch2 as a double


% --- Executes during object creation, after setting all properties.
function w0_Ch2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to w0_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Veff_Ch2_Callback(hObject, eventdata, handles)
% hObject    handle to Veff_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Veff_Ch2 as text
%        str2double(get(hObject,'String')) returns contents of Veff_Ch2 as a double


% --- Executes during object creation, after setting all properties.
function Veff_Ch2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Veff_Ch2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function w0_Ch1_Callback(hObject, eventdata, handles)
% hObject    handle to w0_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of w0_Ch1 as text
%        str2double(get(hObject,'String')) returns contents of w0_Ch1 as a double


% --- Executes during object creation, after setting all properties.
function w0_Ch1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to w0_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function kappa_Ch1_Callback(hObject, eventdata, handles)
% hObject    handle to kappa_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kappa_Ch1 as text
%        str2double(get(hObject,'String')) returns contents of kappa_Ch1 as a double


% --- Executes during object creation, after setting all properties.
function kappa_Ch1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kappa_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function Veff_Ch1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Veff_Ch1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function w0_ChX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to w0_ChX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function kappa_ChX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kappa_ChX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function Veff_ChX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Veff_ChX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function protein_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to protein_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in protein_selectall.
function protein_selectall_Callback(hObject, eventdata, handles)
% hObject    handle to protein_selectall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fnames = get(handles.protein_listbox, 'String');
set(handles.protein_listbox, 'value', [1:length(fnames)]);


% --- Executes on button press in protein_add.
function protein_add_Callback(hObject, eventdata, handles)
% hObject    handle to protein_add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.lastpath
    [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', handles.lastpath ,'MultiSelect', 'off'  );
else
    [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', '' ,'MultiSelect', 'off'  );
end

if ~resfile
    return
end
fnameloc = fullfile(pathname,resfile);
handles.lastpath = pathname;
guidata(hObject, handles);

%% Check for fluorescence intensity files
[pathname, resbase] = fileparts(fnameloc);
% a file containing the fluorescence intensity at the FCS measurement point
flintfile = fullfile(pathname, [resbase '.fint']);
if ~exist(flintfile)
    warndlg(sprintf('File %s not found for \n %s.\n Run fcsImageBrower in ImageJ first', ...
        [resbase '.fint'], fnameloc), 'No fluorescence intensity file found');
    return
end

% check for focalVolume files
if ~get(handles.protein_focvol, 'Value')
    if exist(handles.focVolfile_global)
        focVolfile = handles.focVolfile_global;
    else
        warndlg(sprintf('No focal volume found!\nPerform Fit Dye or try to use local focal volume file'));
        return
    end
else
    focVolfile = getAllFiles(pathname, 'txt', 'focalVolume.txt');
    if length(focVolfile) > 1 % multiple files
        focVolfile = [];
    end
    if isempty(focVolfile)
        focVolfile = [];
    else
        focVolfile = focVolfile{1};
    end
    if isempty(focVolfile)
        [focVolfile, pathnamefocVol] = uigetfile({'*.txt'; '*.*'},'No or multiple focaleVolume.txt found. Select a single focal volume file', pathname);
        if ~focVolfile
            return
        else
            focVolfile = fullfile(pathnamefocVol, focVolfile);
        end
    end
end


fnames = get(handles.protein_listbox, 'String');
if isempty(fnames)
    fnames = {fnameloc};
    handles.flintfiles = {flintfile};
    handles.focalVolfiles = {focVolfile};
else
    fnames = {fnames{:}, fnameloc};
    handles.flintfiles = {handles.flintfiles{:}, flintfile};
    handles.focalVolfiles = { handles.focalVolfiles{:}, focVolfile};
end

set(handles.protein_listbox, 'String', fnames);
set(handles.protein_listbox, 'Value', length(fnames));
guidata(hObject, handles);



% --- Executes on button press in protein_rm.
function protein_rm_Callback(hObject, eventdata, handles)
% hObject    handle to protein_rm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fnames = get(handles.protein_listbox, 'String');
if length(fnames) == 0
    return
end
idx = get(handles.protein_listbox, 'Value');
if idx > 1
    set(handles.protein_listbox, 'Value', idx -1);
end
fnames(idx) = [];
handles.flintfiles(idx) = [];
handles.focalVolfiles(idx) = [];
set(handles.protein_listbox, 'String', fnames);
guidata(hObject, handles);


% --- Executes on button press in protein_clear.
function protein_clear_Callback(hObject, eventdata, handles)
% hObject    handle to protein_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.protein_listbox, 'String', {});
handles.flintfiles = {};
handles.focVolfiles = {};
guidata(hObject, handles);

% --- Executes on button press in protein_fit.
function protein_fit_Callback(hObject, eventdata, handles)
% hObject    handle to protein_fit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(findall(0,'Tag','TMWWaitbar'))
fnames = get(handles.protein_listbox, 'String');
if length(fnames) == 0
    warndlg('Add one res file to fit!');
    return
end
selectedItems = get(handles.protein_listbox, 'value');
fnames_sel = fnames(selectedItems);
focalVolume_sel = handles.focalVolfiles(selectedItems);
fint_sel = handles.flintfiles(selectedItems);
set(handles.protein_abort, 'Enable', 'on');
set(handles.protein_abort, 'Value', 0);
drawnow();
if length(fnames_sel)>0
    force = get(handles.protein_force, 'Value');
    set(handles.figure1, 'pointer', 'watch');
    set(handles.process_textbox, 'String', 'Processing protein ...')
    drawnow();
    pause(0.5);
    for i = 1:length(fnames_sel)
        set(handles.process_textbox, 'String', sprintf('Processing protein %d/%d...', i, length(fnames_sel)))
        handles.progressbar = uiProgressBar('fighandle', handles.statusbar, 'progress', 0.0);
        try
            if handles.protein_model.Value
                workflow_fit_protein(fnames_sel{i}, 'FAsession', get(handles.protein_session, 'String'), ...
                    'focVolfile', focalVolume_sel{i}, 'flintfile', fint_sel{i}, 'progresshandle', handles.progressbar, ...
                    'aborthandle', handles.protein_abort, 'force', get(handles.protein_force, 'Value'), ...
                    'weight', get(handles.protein_weight, 'Value'), 'wtfolder', get(handles.protein_WT, 'String'));
            end
            if handles.dye_model.Value
                workflow_fit_dye_experiment(fnames_sel{i}, 'FAsession', get(handles.protein_session, 'String'), ...
                    'focVolfile', focalVolume_sel{i}, 'flintfile', fint_sel{i}, 'progresshandle', handles.progressbar, ...
                    'aborthandle', handles.protein_abort, 'force', get(handles.protein_force, 'Value'), ...
                    'weight', get(handles.protein_weight, 'Value'), 'wtfolder', get(handles.protein_WT, 'String'));
            end
        catch err
            if ~strcmp(err.identifier, 'workflow_fit_protein:abort')
                getReport(err)
            end
        end
    end
    set(handles.figure1, 'pointer', 'arrow');
    set(handles.protein_abort, 'Value', 0);
    set(handles.protein_abort, 'Enable', 'on');
    set(handles.process_textbox, 'String', '')
    handles.progressbar = uiProgressBar('fighandle', handles.statusbar);
    guidata(hObject, handles);
    pause(0.5);
    drawnow();
end




% --- Executes on button press in protein_abort.
function protein_abort_Callback(hObject, eventdata, handles)
% hObject    handle to protein_abort (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of protein_abort



% --- Executes during object creation, after setting all properties.
function protein_session_CreateFcn(hObject, eventdata, handles)
% hObject    handle to protein_session (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function fontsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fontsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_fontsize.
function popup_fontsize_Callback(hObject, eventdata, handles)
% hObject    handle to popup_fontsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fs = cellstr(get(hObject,'String'));
fs = fs{get(hObject,'Value')};

set(findall(handles.figure1,'-property','FontSize'),'FontSize',str2num(fs));

% --- Executes during object creation, after setting all properties.
function popup_fontsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_fontsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function eta = computeViscosity(T)
% from https://wiki.anton-paar.com/en/water/
visc = [18	1.0526;
    19	1.0266;
    20	1.0016;
    21	0.9775;
    22	0.9544;
    23	0.9321;
    24	0.9107;
    25	0.89;
    26	0.8701;
    27	0.8509;
    28	0.8324;
    29	0.8145;
    30	0.7972;
    31	0.7805;
    32	0.7644;
    33	0.7488;
    34	0.7337;
    35	0.7191;
    36	0.705;
    37	0.6913;
    38	0.678;
    39	0.6652;
    40	0.6527;
    45	0.5958];
eta = interp1(visc(:,1), visc(:,2), T);


function D = computeDiffusion_coeff(T, D_r,  T_r)
% D_r diffusion coefficient at reference temperature T_r
    T0 = 273.15;
    if nargin < 3
        T_r = 27;
    end
    visc = computeViscosity(T);
    visc_r = computeViscosity(T_r);
    % Convert to Kelvin
    T = T + T0;
    T_r = T_r + T0;
    D = D_r*visc_r/visc*T/T_r;
    
