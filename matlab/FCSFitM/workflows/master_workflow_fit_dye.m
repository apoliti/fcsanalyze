function master_workflow_fit_dye(wd)
% MASTER_WORKFLOW_FIT_DYE calls workflow_fit_dye for several directories
% specified in the script
% INPUT:
%   wd - working directory to search recursively for folder named Alexa
% OUTPUT:
%   the function does not give an output. It creates the file wd/ProcessingHelpFiles/alexaDirectories.mat
%   The mat file contains a list of the directories to process.
% When using the GUI this function is not required.
% Antonio Politi, EMBL, January 2017
if nargin < 1
    wd = uigetdir('.', 'Specify main directory where to search for Alexa dye folders');
    
    %wd = 'Z:\AntonioP_elltier1\CelllinesImaging\MitoSysPipelines';
    cd(wd);
end
if ~exist(fullfile(wd, 'ProcessingHelpFiles'))
    mkdir(fullfile(wd, 'ProcessingHelpFiles'))
end
    
if ~exist(fullfile(wd, 'ProcessingHelpFiles', 'alexaDirectories.mat'))
    [indirCell] = getAllFolders(wd, 'Alexa');
    save(fullfile(wd, 'ProcessingHelpFiles', 'alexaDirectories.mat'), 'indirCell');
else
    load(fullfile(wd, 'ProcessingHelpFiles', 'alexaDirectories.mat'));
end
% output subdiretoy to store results
outputdir = 'optimisedFit';

% range to fit kappa
kappaVal = [2:0.1:9]';

force = 0;
% for idir = 36:length(indirCell)
%     indir = indirCell{idir};
%     display(['master_workflow_fit_dye processing directory ' num2str(idir) ' ' indir])
%     try
%         workflow_fit_dye(indir, kappaVal, outputdir, '1c', force);
%     catch
%         display(['failed master_workflow_fit_dye processing directory ' num2str(idir) ' ' indir ' ']);
%     end
% end
