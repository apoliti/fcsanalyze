# workflows
Contains worfklows to fit dye ACF and protein ACF and XCF. A workflow is a sequence of steps to fit a particular function.

## workflow_fit_dye
Workflow is performs a 1 component fit. 
1. Find best tauT for all dye measurements of one experiment at a given kappa
2. Fix tauT and fit N, tauD1, and thetaT for different values of kappa.
3. Find kappa that yields the lowest mean Chi2 (each Chi2 is normalized)
4. Repeat 1-3 with always an improved starting kappa value (this is repeated ~ 3 times)

The file **master_workflow_fit_dye** runs the workflow_fit_dye on all directories named Alexa below a main 
directory.  This file is not required when using the GUI.

## workflow_fit_protein
The workflow performs a 1 and 2 component fitting using anomalous diffusion model and writes results to a
*1c_opt.res/*2c_opt.res file of FA format. Also the corresponding xml files compatible with FA are created. 

The file **master_workflow_fit_protein** runs the workflow_fit_protein on all directories named Alexa below a main directory. This file is not required when using the GUI.
