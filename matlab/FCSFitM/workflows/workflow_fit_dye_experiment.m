function outfiles = workflow_fit_dye_experiment(resfile,  varargin)
%% WORKFLOW_FIT_PROTEIN perform 1 and 2 diffusing components fitting of protein data
%   Use the xml files specified in the res file from FluctuationAnalyzer (FA). Saved results to res files
%   (with effective volume and concentrations) and FA compatible xml files with session name 1c_opt and 2c_opt.
%   If output files (1c_opt.res and 2c_opt.res) exist data is reprocessed
%   only with the option force is on.
%
%   [OUTFILES] = WORKFLOW_FIT_PROTEIN() prompt for a resfile to process. The xml files/fasession are
%   identified by prefix of filename (resfile = 2c.res -> fasession = 2c).
%   Search for focal volume file in subdirectories.
%   [OUTFILES] = WORKFLOW_FIT_PROTEIN(RESFILE) process RESFILE. The xml files/fasession are
%   identified by prefix of filename (resfile = 2c.res -> fasession = 2c). Search for focal volume
%   file in subdirectories.
%
%   INPUT:
%       RESFILE    - a result file containing the directory where to extract files. Software will prompt for focal volume file
%
%   OPTIONS:
%       focvolfile - Name of file containing focal volume data
%       fasession  - The name of the session for finding the xml files. e.g. 0001_R1_P2_K1_Ch2.zen.fasession.xml
%       flintfile  - Name of file containing fluorescence intensity data
%       force      - resForce reprocess even when output files have been found.
%       weight     - Use weighted least-squares
%       WTname     - name of WT directory
%       aborthandle - a handle from a ui element that returns a value of 0 or 1. For instance a toggle element. Default not used
%       progresshandle - a handle from a ui element for a progressbar
%
%   OPTIONS USAGE:
%       e.g. WORKFLOW_FIT_PROTEIN(resfile, focvolfile, 'fasession', '1c')
%
%    OUTPUT:
%       OUTFILES   - path to output resfiles with results of fit
%
% The workflow performs a 1 and 2 component fitting using anomalous diffusion model and writes results to a
% *1c_opt.res/*2c_opt.res file of FA format. Also the xml files are created
% with ID *1c_opt and *2c_opt.
% FOCVOLFILE is a tab delimited txt file. The file is created by
% workflow_fit_dye and named focalVolume.txt
%   "w0_um	Vol_fl	kappa
%   0.1792	0.1858	5.8
%   0.2174	0.3319	5.8
%   0.1974	0.2484	5.8"
% The rows are for Ch1, Ch2 and Cross
% FOCVOLFILE is not necessary for the fitting a default value for kappa of 5.5 will be used instead. The workflow will try to find one or
% just proceed and does not output volume to in *.res file.
%
% Antonio Politi, EMBL, January 2017, Modified March 2017 for

%% read the inputs
if nargin < 1
    % prompt for a directory
    [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', '');
    if ~resfile
        return
    end
    resfile = fullfile(pathname, resfile);
end

if ~exist(resfile)
    display(['Could not find resfile' resfile]);
    return
end

% default file for fluorescence intensity
[pathname, resbase, ext] = fileparts(resfile);
[tmp, dirname] = fileparts(pathname);
defaultFlintfile = fullfile(pathname, [resbase '.fint']);

% parse additional parameters
parin = inputParser;
addRequired(parin, 'resfile', @ischar);
addOptional(parin, 'focvolfile', []);
addOptional(parin, 'fasession', '2c', @ischar);
addOptional(parin, 'force', 0, @isnumeric); % force reprocess
addOptional(parin, 'weight', -1, @isnumeric); %force weighting of fit
addOptional(parin, 'flintfile',  defaultFlintfile, @ischar);
addOptional(parin, 'wtfolder', 'WT', @ischar);
addOptional(parin, 'stdCorr', 1/sqrt(20), @isnumeric); % Scales the std from FA. This yields correct std as it accounts for the 20 windows used to estimate the std. It is the std of the mean
addOptional(parin, 'aborthandle', -1,  @(x) validateattributes(x, {'numeric', 'handle'}, {'nonempty'}));        % a toggle button of a GUI that if on stops computation
addOptional(parin, 'progresshandle', -1,  @(x) validateattributes(x, {'numeric', 'handle'}, {'nonempty'}));     % a handle to uiProgressBar element

parse(parin,resfile,  varargin{:});
parin = parin.Results;


% check for focal volume file and evnetually prompt for a file
if ~exist(parin.focvolfile, 'file')
    warning('No path to focalVolume.txt provided. Try to parse directory/subdirectory and find file');
    % search recursively if not specified
    fnames = getAllFiles(pathname, 'txt', 'focalVolume.txt');
    
    if length(fnames) > 1 || isempty(fnames)
        [fname, pathnamefocVol] = uigetfile({'*.txt'; '*.*'},'Select focalVolume.txt file', fileparts(resfile));
        if ~fname
            warning('No focal volume have been found. Volume data are not integrated in result file');
        end
        parin.focvolfile = fullfile(pathnamefocVol, fname);
    else
        parin.focvolfile = fnames{1};
    end
end

%% Initialization of data
[restable1c, restable2c, focVol] = initializeData(parin.resfile, parin.focvolfile, parin.flintfile);

% get names of xml files
xmlFAfiles = strrep(restable2c.FullPath, ['.zen'], ['.zen.' parin.fasession '.xml']);

%% load model class
clear('MO');
MO = dyeFcsmodel();
MO.tauBoundary = [2, 100000];
MO.maxfittry = 3; %3 repetitions for each fit using random initial conditions around guessed value

if isempty(xmlFAfiles)
    display('No FA xml found use .fcs file instead');
    cond = 3;
else
    cond = 2;
end

%% Set fit condition
% in FA seems that unweighted fit is used. The residuals between matlab fit and FA fit look
% close to each other when unweighted is used. Also the first point is
% often out of fit. Unfortunately the exact measure on how the fit is
% performed is not available. If we take best fit here and plugin into FA
% the Chi2 slightly increases
switch cond
    case 1 %fit using data from FA weighted with standard deviation
        MO.weightChi2 =  1;
        ZEN = 0;
    case 2 %fit using data from FA unweighted
        MO.weightChi2 = 0;
        ZEN = 0;
    case 3 %fit using data from .fcs ZEN files
        MO.weightChi2 = 0; % this is unweighted by definition as std is not available for .fcs
        ZEN = 1;
end

if parin.weight >= 0 && parin.weight < 2 && ZEN == 0
    MO.weightChi2 = parin.weight;
end

%% set name of output files
weightStr = {'','_w'};
outfiles = {fullfile(fileparts(resfile), ['1c_opt' weightStr{MO.weightChi2+1} '.res']), ...
    fullfile(fileparts(resfile), [parin.fasession '_opt' weightStr{MO.weightChi2+1} '.res'])};

if exist(outfiles{1}, 'file') && exist(outfiles{2}, 'file') && ~parin.force
    return
end

%% load data into cell array dataC
display(['Load correlation data for ' resfile])
if ZEN
    dataC = MO.readZeissfcs(zenfiles{1});
    nrData = length(dataC);
else
    nrData = length(xmlFAfiles);
end
%% Workflow

% a wait bar to track progress and abort computation. In case workflow is
% embedded in GUI a different handle can be used (generated from
% uiProgressbar)
if ~ishandle(parin.progresshandle)
    hwaitbar = waitbar(0,sprintf('resfile %s\nfocvolfile %s',  resfile, parin.focvolfile),'Name',['Fitting ' num2str(nrData) ' FCS traces...'],...
        'CreateCancelBtn',...
        'setappdata(gcbf,''canceling'',1)');
end

% perform sanity check if all xml files exist
dataToProcess = [];
isdata_vec = [];
if ~ZEN
    for iD = 1:nrData
        if ~exist(xmlFAfiles{iD})
            % search locally
            [m, iend] = regexp(xmlFAfiles{iD}, dirname, 'match', 'end');
            if iend > 0
                xmlfile = fullfile(pathname, xmlFAfiles{iD}(iend+2:end));
            end
        else
            xmlfile = xmlFAfiles{iD};
        end
        if ~exist(xmlfile, 'file')
            warning(['was not able to find ' xmlfile]);
            continue
        end
        resfile_split = split(resfile, filesep);
        cellnames = split(strrep(xmlfile, fileparts(resfile), resfile_split{end-1}), filesep);
        if ~any(strcmp(cellnames, parin.wtfolder))
            isdata_vec = [isdata_vec iD];
        end
        dataToProcess = [dataToProcess iD];
    end
end



%% load data to estimate tauT and thetaT one component fit global fit
dataC_tau = {};
for iD = isdata_vec
    if ~exist(xmlFAfiles{iD})
        % search locally
        [m, iend] = regexp(xmlFAfiles{iD}, dirname, 'match', 'end');
        if iend > 0
            xmlfile = fullfile(pathname, xmlFAfiles{iD}(iend+2:end));
        end
    else
        xmlfile = xmlFAfiles{iD};
    end
    if ~exist(xmlfile, 'file')
        error(['was not able to find ' xmlfile]);
    end
    data = MO.readFAxml({xmlfile});
    data = data{1};
    % if no fit
    dataC_tau{end+1} = data;
end

for iCh = 1:2
    %%
    
    if iCh == 3
        Chstr = 'Cross';
    else
        Chstr = ['Ch' num2str(iCh)];
    end
    %%
    MO.par.tauT = 7;
    MO.par.thetaT = 0.2;
    MO.par.kappa = focVol.kappa(iCh);
    [thetaT, tauT] = MO.fitThetaTtauTArray(dataC_tau, MO.par.kappa, iCh);
end

MO.par.tauT = tauT;
MO.par.thetaT = thetaT;

for iD = dataToProcess
    if ZEN
        data = dataC{iD};
        xmlfile = [];
        cellnames = split(strrep(zenfiles{1}, fileparts(resfile), []), filesep);
        if any(strcmp(cellnames, parin.wtfolder))
            iswt = 1;
        else
            iswt = 0;
        end
    else
        if ~exist(xmlFAfiles{iD})
            % search locally
            [m, iend] = regexp(xmlFAfiles{iD}, dirname, 'match', 'end');
            if iend > 0
                xmlfile = fullfile(pathname, xmlFAfiles{iD}(iend+2:end));
            end
        else
            xmlfile = xmlFAfiles{iD};
        end
        if ~exist(xmlfile, 'file')
            error(['was not able to find ' xmlfile]);
        end
        % check if file is of WT. in this case set the number of molecules
        % to 0
        %%
        resfile_split = split(resfile, filesep);
        
        cellnames = split(strrep(xmlfile, fileparts(resfile), resfile_split{end-1}), filesep);
        %%
        if any(strcmp(cellnames, parin.wtfolder))
            iswt = 1;
        else
            iswt = 0;
        end
        % check if path contains name
        % this avoids to load huge amount of data in memory
        data = MO.readFAxml({xmlfile});
        data = data{1};
    end
    
    % update progress status bar
    if ishandle(parin.progresshandle) %use external status bar
        parin.progresshandle = uiProgressBar('axeshandle', parin.progresshandle, 'progress', iD/nrData);
        pause(0.5);
    elseif exist('hwaitbar', 'var')  %use waitbar as status bar
        waitbar(iD/nrData, hwaitbar)
        if getappdata(hwaitbar,'canceling')
            delete(hwaitbar)
            outfiles = []
            return
        end
    end
    
    % check if computation should abory
    if ishandle(parin.aborthandle)
        pause(0.5);
        if get(parin.aborthandle, 'Value')
            error('workflow_fit_protein:abort', 'Fit has been stopped by the user');
        end
    end
    
    % Fit for all channels (if CC or XC is found)
    outpar1c = repmat(struct2array(MO.par),3,1); % parameters for 1 component fit
    outpar2c = repmat(struct2array(MO.par),3,1); % parameters for 2 component fit
    
    for iCh = 1:3 %run for all channels
        if isempty(focVol)
            MO.par.kappa = 5.5;
        else
            MO.par.kappa = focVol.kappa(iCh);
        end
        restable1c(iD, ['P09Value' Chstr]) = table(MO.par.kappa);
        
        if isempty(data)
            continue
        end
        if data(1,iCh*2) == 0 % exclude if no data available
            continue
        end
        

        % norm1c is sum of squared residuals used in the fit. norms1c = [unweighted, weighted norm]
        % R2_1c is from norm used for the fit. R2s_1c:[R2_unweighted,
        % R2_weighted norm]
        data(:,2*iCh+1) = data(:,2*iCh+1)*parin.stdCorr;
        [outpar1c(iCh,:), norm1c, R2_1c, norms1c, R2s_1c, Nrpts1c, nrpara1c] = MO.fitOneComponent(data(:,[1 2*iCh 2*iCh+1]), iCh==3);
        [outpar2c(iCh,:), norm2c, R2_2c, norms2c, R2s_2c, Nrpts2c, nrpara2c] = MO.fitTwoComponents(data(:,[1 2*iCh 2*iCh+1]), iCh==3);
        
        % adjusted R2s as used in FA
        R2adj_1c = 1 - (1-R2s_1c).*(Nrpts1c-1)./(Nrpts1c - nrpara1c - 1);
        R2adj_2c = 1 - (1-R2s_2c).*(Nrpts2c-1)./(Nrpts2c - nrpara2c - 1);
        
        %% compute model comparison statistics
        % use for AIC the weighted norm (2nd endtry
        [PAIC, BIC, pvalueChi, pvalueFtest] = modelCompare([norms1c(2), norms2c(2)], [Nrpts1c, Nrpts2c], [nrpara1c, nrpara2c]);
        if iswt
            outpar1c(iCh, 1) = 0;
            outpar2c(iCh, 1) = 0;
        end
        for ipar = 1:length(outpar2c)
            % N, thetaT, tauT, f1, tauD1, alpha1, tauD2, alpha2, kappa
            restable1c(iD, ['P0' num2str(ipar) 'Value' Chstr]) = table(outpar1c(iCh, ipar));
            restable2c(iD, ['P0' num2str(ipar) 'Value' Chstr]) = table(outpar2c(iCh, ipar));
        end
        %% only report weighted norm divided by the nr points
        restable1c(iD, ['ChiSq' Chstr]) = table(norms1c(2)/Nrpts1c);
        restable2c(iD, ['ChiSq' Chstr]) = table(norms2c(2)/Nrpts2c);
        %report unweighted R2 as the weight are largely overestimated. This
        restable1c(iD, ['RSqAdj' Chstr]) = table(R2adj_1c(1));
        restable2c(iD, ['RSqAdj' Chstr]) = table(R2adj_2c(1));
        if ~isempty(focVol)
            %correction for CrossCorr
            corr = table2array(restable1c(iD, ['Total' Chstr]));
            restable1c(iD, ['P13Value' Chstr]) = table(outpar1c(iCh,1)/focVol.Vol_fl(iCh)/0.6022140); % C_nM concentration
            restable1c(iD, ['P14Value' Chstr]) = table(outpar1c(iCh,1)*corr/focVol.Vol_fl(iCh)/0.6022140); %Ccorr_nM
            
            corr = table2array(restable2c(iD, ['Total' Chstr]));
            restable2c(iD, ['P13Value' Chstr]) = table(outpar2c(iCh,1)/focVol.Vol_fl(iCh)/0.6022140); % C_nM concentration
            restable2c(iD, ['P14Value' Chstr]) = table(outpar2c(iCh,1)*corr/focVol.Vol_fl(iCh)/0.6022140); %Ccorr_nM
            if iCh == 3 % this can't be processed now. Need to read all other parameters
                restable1c(iD, ['P13Value' Chstr]) = table(-1); % C_nM concentration
                restable1c(iD, ['P14Value' Chstr]) = table(-1); %Ccorr_nM
                restable2c(iD, ['P13Value' Chstr]) = table(-1); % C_nM concentration
                restable2c(iD, ['P14Value' Chstr]) = table(-1); %Ccorr_nM
            end
        end
        
        restable1c(iD, ['P15Value' Chstr]) = table(pvalueChi);
        restable1c(iD, ['P16Value' Chstr]) = table(pvalueFtest);
        restable1c(iD, ['P17Value' Chstr]) = table(PAIC(1));
        restable1c(iD, ['P18Value' Chstr]) = table(BIC(1)-BIC(2));
        
        restable2c(iD, ['P15Value' Chstr]) = table(pvalueChi);
        restable2c(iD, ['P16Value' Chstr]) = table(pvalueFtest);
        restable2c(iD, ['P17Value' Chstr]) = table(PAIC(2));
        restable2c(iD, ['P18Value' Chstr]) = table(BIC(2)-BIC(1));
    end
    % write updated xml file
    
    for ifit = 1:2
        if isempty(xmlfile)
            continue
        end
        if isempty(data)
            continue
        end
        node = xmlFA.readnode(xmlfile);
        [path1, fout, ext] = fileparts(xmlfile);
        [path, fout, ext] = fileparts(fout);
        
        fname_out = fullfile(path1, sprintf('%s.%dc_opt%s.xml', fout, ifit, weightStr{MO.weightChi2+1}));
        
        idxs = MO.getDataIndexes(data, MO.tauBoundary);
        Ntfits = length(idxs(1):length(data)-idxs(2));
        fit = zeros(Ntfits, 3);
        if ifit == 1
            par = outpar1c;
        else
            par = outpar2c;
        end
        for iC = 1:3
            if (data(1,iC*2) == 0) % do not process if channel has not been acquired
                continue
            end
            fit(:,iC) = MO.Gcomp(MO.parvecToparstruct(par(iC,:)), data(idxs(1):end-idxs(2),1));
        end
        
        xmlFA.writepar(node, par, struct2array(MO.lb), struct2array(MO.hb));
        xmlFA.writefit(node, fit, idxs, MO.model);
        xmlFA.writenode(fname_out, node);
    end
end

%%
tmpfiles = {fullfile(fileparts(resfile), ['1c_tmp.res']), ...
    fullfile(fileparts(resfile), [parin.fasession '_tmp.res'])};


tables = {restable1c, restable2c};
for ifile = 1:2
    writetable(tables{ifile}, tmpfiles{ifile},  'WriteVariableNames',false, 'Delimiter', '\t','FileType', 'text');
    % prepend first 2 lines to keep compatibility
    filepre = fopen(resfile);
    fileout = fopen(outfiles{ifile}, 'w');
    for i = 1:2
        tline = fgetl(filepre);
        if i==2
            tline = strsplit(tline, '\t');
            tline = strjoin(tline(1:141), '\t');
            % add extra column name for fluorescence intensity and class
            str1 = strjoin(restable1c.Properties.VariableNames(142:end), '\t');
            tline = strjoin({tline, str1}, '\t');
        end
        fwrite(fileout, sprintf('%s\n', tline ) );
    end
    fclose(filepre);
    fclose(fileout);
    appendFiles(tmpfiles{ifile}, outfiles{ifile});
    delete(tmpfiles{ifile});
end
if exist('hwaitbar', 'var')
    delete(hwaitbar)
end
end


function  [PAIC, BIC, pvalueChi, pvalueF] = modelCompare(norms, nrpts, nrpara)
% MODELCOMPARE compute stats for comparing 1 and 2 component model

% corrected AIC (AIC will select tendentially larger models than BIC,
% so BIC is more strict)
% low AIC is better
AIC =  2*nrpara + norms + 2*(nrpara+1).*(nrpara)./(nrpts-nrpara-2);
PAIC = exp((min(AIC) - AIC)/length(AIC)); % probability of model with higher AIC to be correct. A value of 1 means that current model has
% lowest AIC
% PAIC(1) gives the probability

% BIC. The lowest value is better. Table provide difference to other model. A negative value means that current model is better
BIC = log(nrpts).*nrpara + norms;

% log-likelihood ratio test. Lower pvalue indicates that more
% complicated model can describe the data
Dlog = norms(1) - norms(2);
df = nrpara(2) - nrpara(1);
pvalueChi = 1 - chi2cdf(Dlog, df);
%%
% a low pValue indicate 2c model is probable
% F-test. Keep in mind that in the original FA
% the sum(weighted_squares) is not ~ Nrpts.
% so we are overestimating the error.
% we reassign the norm using 1/sqrt(20). This is the proper
% rescaling of the std. std should be standard error of the mean of 20
% equally spaced windows.
F = (Dlog/df)/(norms(2)/(nrpts(1)-nrpara(2)));
pvalueF = 1 - fcdf(F, df, nrpts(1)-nrpara(2));
end

function status = appendFiles( readFile, writtenFile )
% APPENDFILES append readFile to writtenFile
fr = fopen( readFile, 'rt' );
fw = fopen( writtenFile, 'at' );
while feof( fr ) == 0
    tline = fgetl( fr );
    fwrite( fw, sprintf('%s\n',tline ) );
end
fclose(fr);
fclose(fw);
end


function [restable1c, restable2c, focVol] = initializeData(resfile, focvolfile, flintfile)
% read focal volume table
if exist(focvolfile, 'file')
    focVol = readtable(focvolfile, 'Delimiter', '\t', 'FileType', 'text');
else
    focVol = [];
end

% read FA result table
if exist(flintfile, 'file')
    flintT = readtable(flintfile,'Delimiter', '\t','FileType', 'text');
else
    flintT = [];
end

% get folder name where resfile is stored
[tmp, upperfolder] = fileparts(fileparts(resfile));

% read resultfile
restable2c = readtable(resfile, 'Delimiter', '\t', 'Header', 1, 'FileType', 'text', 'TreatAsEmpty',{'NA'});


% focal volume entries
restable2c.P11Name = repmat({'w0_um'}, length(restable2c.P11Name),1);
restable2c.P12Name = repmat({'Veff_fl'}, length(restable2c.P11Name),1);
if ~isempty(focVol)
    for iCh = 1:3
        if iCh == 3
            Chstr = 'Cross';
        else
            Chstr = ['Ch' num2str(iCh)];
        end
        restable2c(:,['P11Value' Chstr] ) = repmat({focVol.w0_um(iCh)}, length(restable2c.P11Name),1);
        restable2c(:,['P12Value' Chstr] ) = repmat({focVol.Vol_fl(iCh)}, length(restable2c.P11Name),1);
    end
end
restable2c.P13Name = repmat({'C_nM'}, length(restable2c.P11Name),1);
restable2c.P14Name = repmat({'Ccorr_nM'}, length(restable2c.P11Name),1);

% stat entries to compare to 2c to 1c
restable2c.P15Name = repmat({'pValueLRT'}, length(restable2c.P11Name),1);
restable2c.P16Name = repmat({'pValueFtest'}, length(restable2c.P11Name),1);
restable2c.P17Name = repmat({'PAIC'}, length(restable2c.P11Name),1);
restable2c.P18Name = repmat({'DBIC'}, length(restable2c.P11Name),1);

% remove remaining columns
for i = 19:25
    restable2c(:,['P' num2str(i) 'Name']) = [];
    restable2c(:,['P' num2str(i) 'ValueCh1']) = [];
    restable2c(:,['P' num2str(i) 'ValueCh2']) = [];
    restable2c(:,['P' num2str(i) 'ValueCross']) = [];
end
restable2c.class = repmat({''},  length(restable2c.P11Name),1);
for px = [1, 3, 5, 9]
    for iCh = 1:2
        restable2c(:,sprintf('flint%dCh%d', px, iCh)) = repmat({0}, length(restable2c.P11Name),1);
    end
end
%% write fluorescence intensity to table
try
    if ~isempty(flintT)
        % new format of flint contains path
        if ismember('Path', flintT.Properties.VariableNames)
            %% TODO move this part to a reader class
            % find fcspt number and path and match with the 2 files
            % path of fcs file
            restablePath = cellfun(@fileparts, restable2c.FullPath, 'UniformOutput', 0);
            idx_upperfolder_restable = strfind(restablePath, upperfolder);
            idx_upperfolder_flint = strfind(flintT.Path, upperfolder);
            for i = 1:length(restablePath)
                restablePath{i} = restablePath{i}(idx_upperfolder_restable{i}(1):end);
            end
            for i = 1:length(flintT.Path)
                flintT.Path{i} = flintT.Path{i}(idx_upperfolder_flint{i}(1):end);
            end
            % name of fcs pt as written in restable2c
            restableFcsPt = regexp(restable2c.FileName(:), '.+_P(\d+)_K.+', 'tokens');
            restableFcsPt = [restableFcsPt{:}];
            restableFcsPt = [restableFcsPt{:}]';
            % fcs pt as written in flintT file
            flintTNameFcsPt = regexp(flintT.xmlFile_ptNr, '(?<fname>.+).xml_P(?<fcspt>\d+)+', 'names');
            % check if path is included
            for i = 1:length(restablePath)
            idx = strcmp(restablePath, flintT.Path{i}).*...
                contains(restable2c.FileName, flintTNameFcsPt{i}.fname).*...
                contains(restableFcsPt, flintTNameFcsPt{i}.fcspt) ;
            idx = find(idx);
            
            if length(idx) == 1
                restable2c.class(idx) = flintT.class(i);
                for px = [1, 3, 5, 9]
                    for iCh = 1:2
                        restable2c(idx,sprintf('flint%dCh%d', px, iCh)) = flintT(i, sprintf('Ch%d_%d_%d_',iCh, px, px));
                    end
                end
            end
            end
    else
        restable2c.class = flintT.class;
        for px = [1, 3, 5, 9]
            for iCh = 1:2
                restable2c(:,sprintf('flint%dCh%d', px, iCh)) = flintT(:, sprintf('Ch%d_%d_%d_',iCh, px, px));
            end
        end
    end
end
catch
    warning('could not create the fluorescence intensity columns');
end
restable1c = restable2c;

end
