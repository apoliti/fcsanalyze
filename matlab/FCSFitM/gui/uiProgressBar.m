function h = uiProgressBar( varargin)
% uiProgressBar: A waitbar that can be embedded in a GUI figure.
% Progressbar only goes in one direction. Axes needs to be reinitiated if a a new process starts
% Code modified from https://stackoverflow.com/questions/5368861/how-to-add-progress-bar-control-to-matlab-gui
% Antonio Politi, EMBL, June 2017
global fg_color bg_color
par = inputParser;

addOptional(par, 'fighandle', -1, @(x) validateattributes(x, {'numeric', 'handle'}, {'nonempty'}));
addOptional(par, 'axeshandle', -1, @(x) validateattributes(x, {'numeric', 'handle'}, {'nonempty'}));
addOptional(par, 'progress', 0, @isnumeric);
parse(par, varargin{:});
par = par.Results;

    
%% bg and foreground color
bg_color = 'w';
fg_color = 'r';

if ishandle(par.axeshandle)
    p = get(par.axeshandle, 'Child');
    x = get(p, 'XData');
    if ~isempty(x)
        if x(3) <= par.progress
            x(3:4) = par.progress;
            set(p, 'XData', x);
            h = par.axeshandle;
            return
        else
            delete(par.axeshandle);
            h = makeProgressBar(par.fighandle, par.progress);
        end
    end
else
    h = makeProgressBar(par.fighandle, par.progress);
end
            
end

function h = makeProgressBar(fighandle, progress)
    if ishandle(fighandle)
        global fg_color bg_color
        h = axes('Units','pixels',...
            'XLim',[0 1],'YLim',[0 1],...
            'XTick',[],'YTick',[],...
            'Color',bg_color,...
            'XColor',bg_color,'YColor',bg_color, ...
            'Parent', fighandle, 'Box', 'on');
        patch([0 0 0 0],[0 1 1 0],fg_color,...
            'Parent', h, ...
            'XData', [0 0 progress progress], ...
            'EdgeColor','none');
    else
       warndlg('Specify fighandle', 'uiProgressBar.m');
       h = -1;
       return 
    end
end