%% Test some of the functions in the xmlFA class and the model class
%% Pathnames
pathmain = mfilename('fullpath');
mainfolder = fileparts(fileparts(pathmain));
addpath(mainfolder, '-end');
addpath(genpath_loc(mainfolder),'-end');
exampleFolder = fullfile(fileparts(fileparts(pathmain)), 'example_data');

%% Read the .fcs file
fname1 = {fullfile(exampleFolder, 'APDsFormat', '488_561_GaAsP.fcs')};
MO = dyeFcsmodel()
[AC, mCounts] = MO.readZeissfcs(fname1);

%% Read multiple .fcs file
fname = {fname1{1}, fullfile(exampleFolder, 'Protein', '0001.fcs')};
MO = dyeFcsmodel()
[AC, mCounts] = MO.readZeissfcs(fname);

%%  Read FA generated xml files
fname = {fullfile(exampleFolder, 'Alexa', '488_561nm_R1_P1_K1_Ch2-Ch1.zen.1c_opt.xml')};
MO = dyeFcsmodel()
AC = MO.readFAxml(fname);

%% xmlFA class reading
fname = fullfile(exampleFolder, 'Protein', '0001_R1_P1_K1_Ch2.zen.1c.xml');
% parameters [N thetaT tauT f1 tauD1 alpha1 tauD2 alpha2 kappa; ....]
par_FA = xmlFA.readpar(fname);
% fit  [tau_us fitCh1 fitCh2 fitXC]
fitval = xmlFA.readfit(fname);
% ACF and XCF  tau AC1 AC1_error AC2 AC2_error XC XC_error] error must be scaled by 1/sqrt(20)
acfval = xmlFA.readcor(fname);
%% compare fit from 
MO = proteinFcsmodel();
par_FA = xmlFA.readpar(fname_FA);


dataC = MO.readFAxml({fname_FA});
idxs = MO.getDataIndexes(dataC{1}, MO.tauBoundary);
fitFA = MO.Gdiffcomp(par_FA(1,:), dataC{1}(idxs(1):end-idxs(2),:), 0);
fitFA_w = MO.Gdiffcomp(par_FA_w(1,:), dataC{1}(idxs(1):end-idxs(2),:), 0);
fitML = MO.Gdiffcomp(par_ML(1,:), dataC{1}(idxs(1):end-idxs(2),:), 0);
fitML_w = MO.Gdiffcomp(par_ML_w(1,:), dataC{1}(idxs(1):end-idxs(2),:), 0);

semilogx(dataC{1}(idxs(1):end-idxs(2),1), [fitFA fitFA_w fitML fitML_w]);
ylabel('residuals')
xlabel('tau (us)')
legend('fitFA','fitFA weighted', 'fitML', 'fitML weighted')
%%
xmlFA.readcor(fname_in);

%%
fit = xmlFA.readfit(fname_in);

%% write fit to xml file
clear('MO');
MO = dyeFcsmodel();
dataC = MO.readFAxml({fname_in});
%fit Ch1
parf1 = MO.fitThetaTtauT(dataC{1}(:, [1 2 3]), 5);
%fit Ch2
parf2 = MO.fitThetaTtauT(dataC{1}(:, [1 4 5]), 5);
idxs = MO.getDataIndexes(dataC{1}, MO.tauBoundary);
fit1 = MO.Gcomp(MO.parvecToparstruct(parf1), dataC{1}(idxs(1):end-idxs(2),1));
fit2  = MO.Gcomp(MO.parvecToparstruct(parf2), dataC{1}(idxs(1):end-idxs(2),1));
node = xmlFA.readnode(fname_in);
xmlFA.writepar(node, [parf1;parf2], struct2array(MO.lb), struct2array(MO.hb));
xmlFA.writefit(node, [fit1  fit2], idxs, MO.model);
xmlFA.writenode(fname_out, node);

