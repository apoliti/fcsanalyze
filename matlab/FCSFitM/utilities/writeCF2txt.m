function writeCF2txt(resfiles, FAsession, outfile, varargin)
% WRITECF2TXT Read FA resfile and collect data from each FA xml file. Pool all ACF (or CCF) for 
%             one channel to a txt file. Data that pass the QC according to Rsq and Chisq values is pooled. 
%   WRITECF2TXT(resfiles, FAsession, outfile)  write results using default parameters
%   WRITECF2TXT(resfiles, FAsession, outfile, 'Ch', 3) write results of Cross correlation
%   WRITECF2TXT(resfiles, FAsession, outfile, 'Ch', 2, 'Chisq_lim', 1.2) write results of channel 2 using specified 
%              value of Chisq upper limit.
%   INPUT:
%       resfiles:   A cell array with path to res files
%       FAsession:  The name of the FA session to use. FA xml files with containing the session name are used. 
%       outfile:    Name of outfile to store the data. The name then gets an additional ID given the channel to store
%   OPTIONS:
%       annot:      Annotation to exclude, a char
%       Chisq_lim:  Upper limit for Chisq of fit, a number
%       Rsq_lim:    Lower limit for Rsq of fit, a number
%       bleach_lim: Lower and upper limit for bleach coefficient for Ch1
%       and Ch2 [low_Ch1, high_Ch1, low_Ch2, high_Ch2]
%   Format output is 
%   tau Ch Ch_error Ch_fit path 
%
% Antonio Politi, EMBL 2018, modified 2019 May

%% Parameter input
% TODO change to example_data folder
if nargin < 1
    resfiles = {'W:\Shotaro\Live-cell-imaging\HeLa-FCS\180112-HeLa-gfpSEH1z76z2_gfpNUP107z26z31\Calibration\2c_opt.res'};
end
if nargin < 2
    FAsession = '2c_opt';
end
if nargin < 3
    outfile = 'W:\Shotaro\Live-cell-imaging\HeLa-FCS\180112-HeLa-gfpSEH1z76z2_gfpNUP107z26z31\Calibration\2c_opt_CF';
end
if ~iscell(resfiles)
    resfiles = {resfiles};
end

p = inputParser;
addOptional(p, 'Ch', [1], @(x) isnumeric(x) & length(x) == 1 & (x == 1 | x == 2 | x == 3)) % Channel to create output for
addOptional(p, 'Chisq_lim', [1.2], @(x) isnumeric(x) & length(x) == 1  ) % UPper limit for ChiSq
addOptional(p, 'Rsq_lim', [0.8], @(x) isnumeric(x) & length(x) == 1  ) % Lower limit for Rsq
addOptional(p, 'annot', 'x', @(x) ischar(x))
addOptional(p, 'bleach_lim', [0.9, 5, 0.9, 5], @(x) isnumeric(x) & length(x) == 4) % lower limit Ch1, upper limit Ch1, lower limitCh1, lower limitCh2
parse(p, varargin{:});
p = p.Results;

%% initialize reader
clear('MO')
MO = proteinFcsmodel();
    
%% Process all res files
for resfile = resfiles
    %% read FA res resfile 
    resfile = resfile{1};
    [pathname, resfilename] = fileparts(resfile);
    [tmp, dirname] = fileparts(pathname);

    [pathname, resbase, ext] = fileparts(resfile);
    restable = readtable(resfile, 'Delimiter', '\t', 'NumHeaderLines', 1, 'FileType', 'text', 'TreatAsEmpty',{'NA'});
    idxAnn = [1:length(restable.Annotation)];
    if iscell(restable.Annotation)
       idxAnn = idxAnn(~strcmp(restable.Annotation, p.annot));
    end
    
    %% get names of xml files and perform sanity check if all xml files exist
    xmlFAfiles = strrep(restable.FullPath, ['.zen'], ['.zen.' FAsession '.xml']);
    nrData = length(xmlFAfiles);
    dataToProcess = [];
    for iD = idxAnn
        xmlfile = xmlFAfiles{iD};
        if ~exist(xmlfile, 'file')
            % search locally
            [m, iend] = regexp(xmlfile, dirname, 'match', 'end');
            if iend > 0
                xmlfile = fullfile(pathname, xmlFAfiles{iD}(iend+2:end));
            end
            if ~exist(xmlfile, 'file')
                warning(['was not able to find ' xmlfile]);
                continue
            end
            xmlFAfiles{iD} = xmlfile;
        end
        dataToProcess = [dataToProcess iD];
    end
    if length(dataToProcess) == 0
        return
    end
    
    %% output file
    if p.Ch == 3
        Chstr = 'C';
    else
        Chstr = num2str(p.Ch);
    end
    fid = fopen([outfile '_Ch' Chstr '.txt'], 'w');
    fprintf(fid, 'tau_us\tCh%d\tCh%d_std\tCh%d_fit\tpath\n', p.Ch, p.Ch, p.Ch);

        
    %% Process all data
    for iD =  dataToProcess
        %% Removed bad fits
        if restable{iD,['ChiSqCh' Chstr]} == 0 ||  restable{iD,['ChiSqCh' Chstr]} > p.Chisq_lim
            continue
        end
        if restable{iD,['RSqAdjCh' Chstr]} < p.Rsq_lim
            continue
        end
        
        bleachval = restable{iD,['BleachingCh' Chstr]};
        if p.Ch ~=3
            if bleachval < p.bleach_lim(1 + (p.Ch-1)*2) || bleachval > p.bleach_lim(2 + (p.Ch-1)*2)
                continue
            end
        end
        %% Read xml fie and write output
        display(sprintf('Processing file %d/%d', iD, max(dataToProcess)))
        xmlfile = xmlFAfiles{iD};
        data = xmlFA.readcor(xmlfile);
        fit = xmlFA.readfit(xmlfile);
        data = [data(:,[1 p.Ch*2 p.Ch*2+1]) fit(:, p.Ch + 1)];
        for i = 1:size(data)
            fprintf(fid, '%d\t%e\t%e\t%e\t%s\n', data(i,:), restable.FullPath{iD});
        end
    end
    fclose(fid);
end
end