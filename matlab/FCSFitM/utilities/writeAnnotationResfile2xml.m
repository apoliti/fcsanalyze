function writeAnnotationResfile2xml(resfile, FAsession)
% WRITEANNOTATIONRESFILE2XML
%   Example function how to write Annotation from resfile to xml file and
%   keep compatibility with FA. Useful if annotation has been done on a
%   different system. For instance for Marina's project
%   INPUTS:
%       resfile   - input resfile
%       FAsession - name of the session to write out the result
    currpath = mfilename('fullpath');
    if ~exist('xmlFA')
        addpath(fullfile(fileparts(fileparts(currpath)), 'classes'));
    end
    if nargin < 1
        [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', '');
        if ~resfile
            return
        end
        resfile = fullfile(pathname, resfile);
    end
    [pathname, resbase, ext] = fileparts(resfile);
    logfile = fullfile(pathname, 'writeannotationrefile2xml.log');
    if ~(exist(resfile, 'file') == 2)
        warning('File %s does not exist', resfile);
        fid = fopen(logfile, 'a');
        fprintf(fid,'File %s does not exist\n', resfile);
        fclose(fid);
        return
    end
    [tmp, dirname] = fileparts(pathname);
    if nargin < 2
        FAsession = resbase;
        FAsession = '2c'
    end 
    restable = readtable(resfile, 'Delimiter', '\t', 'Header', 1, 'FileType', 'text', 'TreatAsEmpty',{'NA'});
    xmlFAfiles = strrep(restable.FullPath, ['.zen'], ['.zen.' FAsession '.xml']);
    for iD = 1:length(xmlFAfiles)
        fprintf('%s Writing annotation of file %d/%d\n', resfile, iD, length(xmlFAfiles))
        if ~exist(xmlFAfiles{iD})
            % search locally
            [m, iend] = regexp(xmlFAfiles{iD}, dirname, 'match', 'end');
            xmlfile = fullfile(pathname, xmlFAfiles{iD}(iend+1:end));
        else
            xmlfile = xmlFAfiles{iD};
        end
        if ~(exist(xmlfile, 'file') == 2)
            warning('Could not find file %s', xmlfile);
            fid = fopen(logfile, 'a');
            fprintf(fid, 'Could not find file %s\n', xmlfile);
            fclose(fid);
            continue
        end
        try
            node = xmlFA.readnode(xmlfile);
        catch
            warning('Could not read file %s', xmlfile);
            fid = fopen(logfile, 'a');
            fprintf(fid, 'Could not read file %s\n', xmlfile);
            fclose(fid);
            continue
        end
        if iscell(restable.Annotation)
            ann = restable.Annotation{iD};
        else
            ann = restable.Annotation(iD);
        end 
             
        if isnan(ann)
           xmlFA.writeannotation(node, 32);
        else
           xmlFA.writeannotation(node, ann);
        end
        xmlFA.writenode(xmlfile, node);
    end
end

