Parameters used in the Filtering and focal Volume tab
Excluded annotations x
R2_lb 0.9; use 1
Chi2_hb 1.5; use 1
Bleaching_lb 0.9; Bleaching_hb 5.0; use 1
Counts/Offset_lb 1.5; use 1
Conc_hb 3000.0 [nM]; use 1
use mFP 1 ; scale with mFP CPM 1
Veff_Ch1 0.3438; Veff_Ch2 0.0000; Veff_Cross 0.0000 [fl]
Veff_Ch1 0.3438; Veff_Ch 0.0000; Veff_Cross 0.0000 [fl]
w0_Ch1 0.2239; w0_Ch2 0.0000; W0_Cross 0.0000 [um]
kappa 5.5
